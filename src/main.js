const readline = require('readline');
const chalk = require('chalk');
var figlet = require('figlet');
const log = console.log;
var clear = require('clear');
const { createTenant } = require('./database');
const dotenv = require('dotenv');
dotenv.config();
const actions = {
    createTenant: '1',
    close: '2',
}

clear();
const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

figlet('CONVERSSO TOOLS', function(err, data) {
    if (err) {
        return;
    }
    log(chalk.red(data));
    askTheAction();
});

function askTheAction() {
    log(chalk.blue('1. Crear un nuevo tenant'));
    log(chalk.blue('2. Salir'));
    rl.question('Acción:  ', (answer) => {
        switch(answer) {
            case actions.createTenant:
                askTenantData();
            break;
            case actions.close: 
                rl.close();
                break;
            default:
                log();
                askTheAction();
                break;
        }
    });
}

function ask(title) {
    return new Promise((res, rej) => {
        rl.question(`${title}: `, (answer) => {
            res(answer);
        });
    });
}

async function askTenantData() {
    log('');
    const tenantUUID = getUUID();
    const businessName = await ask('Nombre de la empresa');
    const email = await ask('Correo inicial');
    log(chalk.yellow('Creando nuevo tenant'));
    log(chalk.red('Nuevo tenant: ' + tenantUUID));
    await createTenant(tenantUUID, email, businessName);
    askTheAction();
}

function getUUID(){
    var dt = new Date().getTime();
    var uuid = 'xxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = (dt + Math.random()*16)%16 | 0;
        dt = Math.floor(dt/16);
        return (c=='x' ? r :(r&0x3|0x8)).toString(16);
    });
    return uuid;
}