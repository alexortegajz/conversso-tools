
----------> Auth
INSERT INTO public.jhi_user(
	login, password_hash, email, activated, lang_key, reset_key, created_by, created_date, reset_date, last_modified_by, last_modified_date, tenant_uuid)
	VALUES ('$email', '$2a$10$ZK5o75tbiZv8qGo4RcLVPeMm1M6hwphJrjSK77eBEsMWnE0w66HWa', '$email', true, 'es', '75871506776676483885', 'sistemas@ow', now(), now(), 'sistemas@ow', now(), '$tenant');

INSERT INTO public.jhi_user_authority(user_id, authority_name) VALUES (currval(pg_get_serial_sequence('jhi_user','id')) , 'ROLE_ADMIN');
