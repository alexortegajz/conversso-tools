const fs = require('fs');
const { Pool } = require('pg');
const chalk = require('chalk');
const log = console.log;


async function createTenant(tenantUUID, email, businessName) {
    try {
        await runCreateTenantQuery(tenantUUID, email);
        await runAuthQuery(tenantUUID, email);
        await runBillingQuery(tenantUUID, email, businessName);
    } catch(err) {
        log(chalk.red(err));
    }
}

async function runCreateTenantQuery(tenantUUID, email) {
    let createTenantQuery = await readFile('./src/database/conversso-script.sql');
    createTenantQuery = replaceAll(createTenantQuery, '$tenant', tenantUUID);
    createTenantQuery = replaceAll(createTenantQuery, '$email', email);
    await runQuery('ziix', createTenantQuery);
}

async function runAuthQuery(tenantUUID, email) {
    let authQuery = await readFile('./src/database/auth-script.sql');
    authQuery = replaceAll(authQuery, '$tenant', tenantUUID);
    authQuery = replaceAll(authQuery, '$email', email);
    await runQuery('auth', authQuery);
}

async function runBillingQuery(tenantUUID, email, businessName) {
    let billingQuery = await readFile('./src/database/billing-script.sql');
    billingQuery = replaceAll(billingQuery, '$tenant', tenantUUID);
    billingQuery = replaceAll(billingQuery, '$email', email);
    billingQuery = replaceAll(billingQuery, '$businessName', businessName);
    await runQuery('billing', billingQuery);
}

function replaceAll(string, searchParam, replacement) {
    const replacedString = string.replace(searchParam, replacement);
    return string.indexOf(searchParam) !== -1 ? replaceAll(replacedString, searchParam, replacement) : replacedString;
}

async function runQuery(database, query) {
    return new Promise(async (res, rej) => {
        const pool = new Pool({ database });
        try {
            pool.query(query, [], (err, result) => {
                if (err) {
                    rej(err);
                } else {
                    res();
                }
                pool.end();
            });
        } catch(err) {
            rej(err);
        }
    });
}
function readFile(src) {
    return new Promise((res, rej) => {
        fs.readFile(src, 'utf8', function (err,data) {
            if (err) {
                rej(err);
            }
            res(data);
          });
    });
}

module.exports = {
    createTenant,
}