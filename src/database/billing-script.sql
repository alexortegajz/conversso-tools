----------> Billing

INSERT INTO public.tenant(
	name, tenant_uuid, db_name, plan_id, account_type, created_date, expiry_date, agent_licenses, extra_site_licenses, chat_allowed, voip_allowed, active, video_allowed)
	VALUES ('$businessName', '$tenant', '$tenant', 'BASIC', 1, now(), now(), 1, 1, true, true, true, true);

INSERT INTO public.webhook(
	url, request_type, millis, environment)
	VALUES ('https://agent.conversso.com/api/asteriskRobot/$tenant', 'POST', 45000, 'prod');
INSERT INTO public.webhook(
	url, request_type, millis, environment)
	VALUES ('https://manager.conversso.com/api/updateQueueDetails/$tenant', 'POST', 5000, 'prod');
INSERT INTO public.webhook(
	url, request_type, millis, environment)
	VALUES ('https://manager.conversso.com/api/updateQueueSummary/$tenant', 'POST', 45000, 'prod');
