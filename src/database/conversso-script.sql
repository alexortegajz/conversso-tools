

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
-- SET row_security = off;

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';

CREATE SCHEMA "$tenant";
SET search_path = "$tenant", pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;


CREATE TABLE account (
    account_id integer NOT NULL,
    email character varying,
    channel_type integer NOT NULL,
    mail_host character varying,
    mail_username character varying,
    mail_password character varying,
    mail_tls_active character varying,
    social_name character varying,
    social_username character varying,
    picture character varying,
    hostname character varying,
    token character varying,
    token_secret character varying(255),
    deleted boolean,
    port character varying,
    username character varying,
    password character varying,
    url character varying,
    answer_type integer,
    company_address character varying,
    smtp_host character varying,
    smtp_port character varying,
    reply_to character varying,
    default_account boolean DEFAULT false NOT NULL,
    folder_names character varying
);


ALTER TABLE account OWNER TO postgres;

--
-- TOC entry 178 (class 1259 OID 17344)
-- Name: account_account_id_seq; Type: SEQUENCE; Schema: xxxx; Owner: postgres
--

CREATE SEQUENCE account_account_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE account_account_id_seq OWNER TO postgres;

--
-- TOC entry 3283 (class 0 OID 0)
-- Dependencies: 178
-- Name: account_account_id_seq; Type: SEQUENCE OWNED BY; Schema: xxxx; Owner: postgres
--

ALTER SEQUENCE account_account_id_seq OWNED BY account.account_id;


--
-- TOC entry 179 (class 1259 OID 17346)
-- Name: agent; Type: TABLE; Schema: xxxx; Owner: postgres
--

CREATE TABLE agent (
    agent_id integer NOT NULL,
    lastname character varying(45) DEFAULT NULL::character varying NOT NULL,
    firstname character varying(45) DEFAULT NULL::character varying NOT NULL,
    username character varying(45) DEFAULT NULL::character varying NOT NULL,
    deleted boolean DEFAULT false NOT NULL,
    avatar_url character varying,
    extension character varying,
    group_id integer,
    role character varying,
    last_seen_wall_date timestamp without time zone DEFAULT ('now'::text)::timestamp(6) with time zone NOT NULL,
    answer_type integer
);


ALTER TABLE agent OWNER TO postgres;

--
-- TOC entry 180 (class 1259 OID 17357)
-- Name: agent_agent_id_seq; Type: SEQUENCE; Schema: xxxx; Owner: postgres
--

CREATE SEQUENCE agent_agent_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE agent_agent_id_seq OWNER TO postgres;

--
-- TOC entry 3284 (class 0 OID 0)
-- Dependencies: 180
-- Name: agent_agent_id_seq; Type: SEQUENCE OWNED BY; Schema: xxxx; Owner: postgres
--

ALTER SEQUENCE agent_agent_id_seq OWNED BY agent.agent_id;


--
-- TOC entry 181 (class 1259 OID 17359)
-- Name: agent_campaign; Type: TABLE; Schema: xxxx; Owner: postgres
--

CREATE TABLE agent_campaign (
    agent_id integer NOT NULL,
    campaign_id integer NOT NULL,
    agent_campaign_id integer NOT NULL,
    priority integer DEFAULT 3
);


ALTER TABLE agent_campaign OWNER TO postgres;

--
-- TOC entry 182 (class 1259 OID 17363)
-- Name: agent_campaign_agent_campaign_id_seq; Type: SEQUENCE; Schema: xxxx; Owner: postgres
--

CREATE SEQUENCE agent_campaign_agent_campaign_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE agent_campaign_agent_campaign_id_seq OWNER TO postgres;

--
-- TOC entry 3285 (class 0 OID 0)
-- Dependencies: 182
-- Name: agent_campaign_agent_campaign_id_seq; Type: SEQUENCE OWNED BY; Schema: xxxx; Owner: postgres
--

ALTER SEQUENCE agent_campaign_agent_campaign_id_seq OWNED BY agent_campaign.agent_campaign_id;


--
-- TOC entry 183 (class 1259 OID 17365)
-- Name: agent_status_history; Type: TABLE; Schema: xxxx; Owner: postgres
--

CREATE TABLE agent_status_history (
    agent_status_history_id integer NOT NULL,
    datetime timestamp(6) without time zone DEFAULT NULL::timestamp without time zone NOT NULL,
    agent_id integer NOT NULL,
    status_code_id character varying(3) DEFAULT NULL::character varying NOT NULL,
    campaign_id integer
);


ALTER TABLE agent_status_history OWNER TO postgres;

--
-- TOC entry 184 (class 1259 OID 17370)
-- Name: agent_status_history_agent_status_history_id_seq; Type: SEQUENCE; Schema: xxxx; Owner: postgres
--

CREATE SEQUENCE agent_status_history_agent_status_history_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE agent_status_history_agent_status_history_id_seq OWNER TO postgres;

--
-- TOC entry 3286 (class 0 OID 0)
-- Dependencies: 184
-- Name: agent_status_history_agent_status_history_id_seq; Type: SEQUENCE OWNED BY; Schema: xxxx; Owner: postgres
--

ALTER SEQUENCE agent_status_history_agent_status_history_id_seq OWNED BY agent_status_history.agent_status_history_id;


--
-- TOC entry 185 (class 1259 OID 17372)
-- Name: cdr; Type: TABLE; Schema: xxxx; Owner: postgres
--

CREATE TABLE cdr (
    calldate timestamp without time zone NOT NULL,
    clid character varying(80) NOT NULL,
    src character varying(80) NOT NULL,
    dst character varying(80) NOT NULL,
    dcontext character varying(80) NOT NULL,
    channel character varying(80) NOT NULL,
    dstchannel character varying(80) NOT NULL,
    lastapp character varying(80) NOT NULL,
    lastdata character varying(80) NOT NULL,
    duration integer NOT NULL,
    billsec integer NOT NULL,
    disposition character varying(45) NOT NULL,
    amaflags integer NOT NULL,
    accountcode character varying(20) NOT NULL,
    uniqueid character varying(150) NOT NULL,
    userfield character varying(255) NOT NULL,
    peeraccount character varying(20) NOT NULL,
    linkedid character varying(150) NOT NULL,
    sequence integer NOT NULL,
    cdr_id integer NOT NULL
);


ALTER TABLE cdr OWNER TO postgres;

--
-- TOC entry 186 (class 1259 OID 17378)
-- Name: cdr_ticket; Type: TABLE; Schema: xxxx; Owner: postgres
--

CREATE TABLE cdr_ticket (
    uniqueid character varying NOT NULL,
    ticket_id integer NOT NULL
);


ALTER TABLE cdr_ticket OWNER TO postgres;

--
-- TOC entry 187 (class 1259 OID 17384)
-- Name: call; Type: VIEW; Schema: xxxx; Owner: postgres
--

CREATE VIEW call AS
 SELECT cdr.calldate,
    cdr.clid,
    cdr.src,
    cdr.dst,
    cdr.dcontext,
    cdr.channel,
    cdr.dstchannel,
    cdr.lastapp,
    cdr.lastdata,
    cdr.duration,
    cdr.billsec,
    cdr.disposition,
    cdr.amaflags,
    cdr.accountcode,
    cdr.uniqueid,
    cdr.userfield,
    cdr.peeraccount,
    cdr.linkedid,
    cdr.sequence,
    cdr.cdr_id,
    cdr_ticket.ticket_id
   FROM ((cdr
     JOIN ( SELECT cdr_1.uniqueid,
            max(cdr_1.calldate) AS calldate
           FROM cdr cdr_1
          GROUP BY cdr_1.uniqueid) matches USING (uniqueid, calldate))
     LEFT JOIN cdr_ticket ON (((cdr_ticket.uniqueid)::text = (cdr.uniqueid)::text)))
  WHERE ((cdr.dst IS NOT NULL) AND ((cdr.dst)::text <> ''::text));


ALTER TABLE call OWNER TO postgres;

--
-- TOC entry 188 (class 1259 OID 17389)
-- Name: queue_log; Type: TABLE; Schema: xxxx; Owner: postgres
--

CREATE TABLE queue_log (
    queue_log_id integer NOT NULL,
    "time" timestamp without time zone DEFAULT now() NOT NULL,
    created timestamp without time zone DEFAULT now() NOT NULL,
    callid character varying NOT NULL,
    queuename character varying NOT NULL,
    agent character varying NOT NULL,
    event character varying NOT NULL,
    data1 character varying(100) NOT NULL,
    data2 character varying(100) NOT NULL,
    data3 character varying(100) NOT NULL,
    data4 character varying(100) NOT NULL,
    data5 character varying(100) NOT NULL
);


ALTER TABLE queue_log OWNER TO postgres;

--
-- TOC entry 189 (class 1259 OID 17397)
-- Name: call_cdr; Type: VIEW; Schema: xxxx; Owner: postgres
--

CREATE VIEW call_cdr AS
 SELECT ('cdr-'::text || cdr.cdr_id) AS call_id,
    cdr.calldate,
    cdr.uniqueid,
    cdr.src,
    cdr.dst,
    cdr.duration,
    cdr.billsec,
    cdr.disposition,
    cdr_ticket.ticket_id,
    false AS is_queue_call,
    NULL::unknown AS queue_name,
    NULL::unknown AS queue_disposition,
    NULL::unknown AS hold_duration,
    cdr.channel
   FROM (cdr
     LEFT JOIN cdr_ticket ON (((cdr_ticket.uniqueid)::text = (cdr.uniqueid)::text)))
  WHERE (((cdr.dst IS NOT NULL) AND ((cdr.dst)::text <> ''::text)) AND (NOT ((cdr.uniqueid)::text IN ( SELECT DISTINCT qd.callid
           FROM queue_log qd))));


ALTER TABLE call_cdr OWNER TO postgres;

--
-- TOC entry 190 (class 1259 OID 17402)
-- Name: queue_log_xt; Type: VIEW; Schema: xxxx; Owner: postgres
--

CREATE VIEW queue_log_xt AS
 SELECT ql1.uniqueid,
    ql1.queuename,
    (array_agg(ql1.disposition ORDER BY ql1.queue_log_id DESC))[1] AS disposition,
    (array_agg(ql1.created ORDER BY ql1.queue_log_id))[1] AS created,
    (array_agg(ql1.answered ORDER BY ql1.queue_log_id DESC))[1] AS answered,
    (array_agg(ql1.hold_duration ORDER BY ql1.queue_log_id DESC))[1] AS hold_duration,
    (array_agg(ql1.talk_duration ORDER BY ql1.queue_log_id DESC))[1] AS talk_duration,
    (array_agg(ql1.number ORDER BY ql1.queue_log_id))[1] AS number,
    (array_agg(ql1.queue_log_id ORDER BY ql1.queue_log_id DESC))[1] AS queue_log_id,
    (array_agg(ql1.agent ORDER BY ql1.queue_log_id DESC))[1] AS agent
   FROM ( SELECT queue_log.queue_log_id,
            queue_log.agent,
            queue_log.callid AS uniqueid,
            queue_log.queuename,
            queue_log.event AS disposition,
            queue_log.created,
                CASE
                    WHEN ((queue_log.event)::text = 'ABANDON'::text) THEN 'ABANDON'::text
                    WHEN ((queue_log.event)::text = 'ENTERQUEUE'::text) THEN 'ABANDON'::text
                    ELSE 'ANSWERED'::text
                END AS answered,
                CASE
                    WHEN ((queue_log.event)::text = ANY (ARRAY[('ABANDON'::character varying)::text, ('ATTENDEDTRANSFER'::character varying)::text, ('BLINDTRANSFER'::character varying)::text])) THEN queue_log.data3
                    WHEN ((queue_log.event)::text = 'ENTERQUEUE'::text) THEN '0'::character varying(100)
                    ELSE queue_log.data1
                END AS hold_duration,
                CASE
                    WHEN ((queue_log.event)::text = ANY (ARRAY[('ATTENDEDTRANSFER'::character varying)::text, ('BLINDTRANSFER'::character varying)::text])) THEN queue_log.data4
                    WHEN ((queue_log.event)::text = 'ABANDON'::text) THEN '0'::character varying
                    WHEN ((queue_log.event)::text = 'ENTERQUEUE'::text) THEN '0'::character varying
                    ELSE queue_log.data2
                END AS talk_duration,
                CASE
                    WHEN ((queue_log.event)::text = 'ENTERQUEUE'::text) THEN (queue_log.data2)::text
                    ELSE 'N/A'::text
                END AS number
           FROM queue_log
          WHERE (((queue_log.event)::text = ANY (ARRAY[('ENTERQUEUE'::character varying)::text, ('ABANDON'::character varying)::text, ('COMPLETECALLER'::character varying)::text, ('COMPLETEAGENT'::character varying)::text, ('ATTENDEDTRANSFER'::character varying)::text, ('BLINDTRANSFER'::character varying)::text])) AND ((queue_log.data1)::text <> 'LINK'::text))) ql1
  GROUP BY ql1.uniqueid, ql1.queuename;


ALTER TABLE queue_log_xt OWNER TO postgres;

--
-- TOC entry 191 (class 1259 OID 17407)
-- Name: call_queue; Type: VIEW; Schema: xxxx; Owner: postgres
--

CREATE VIEW call_queue AS
 SELECT ('que-'::text || qd.queue_log_id) AS call_id,
    qd.created AS calldate,
    qd.uniqueid,
    qd.number AS src,
    qd.agent AS dst,
    to_number((qd.talk_duration)::text, '99G999D9S'::text) AS duration,
    to_number((qd.talk_duration)::text, '99G999D9S'::text) AS billsec,
    qd.answered AS disposition,
    cdr_ticket.ticket_id,
    true AS is_queue_call,
    qd.queuename AS queue_name,
    qd.disposition AS queue_disposition,
    qd.hold_duration,
    '' AS channel
   FROM (queue_log_xt qd
     LEFT JOIN cdr_ticket ON (((cdr_ticket.uniqueid)::text = (qd.uniqueid)::text)));


ALTER TABLE call_queue OWNER TO postgres;

--
-- TOC entry 192 (class 1259 OID 17412)
-- Name: campaign; Type: TABLE; Schema: xxxx; Owner: postgres
--

CREATE TABLE campaign (
    campaign_id integer NOT NULL,
    name character varying(45) DEFAULT NULL::character varying NOT NULL,
    campaign_type_id character varying(3) DEFAULT NULL::character varying NOT NULL,
    deleted boolean DEFAULT false NOT NULL,
    greeting_active boolean DEFAULT false NOT NULL,
    greeting_message character varying,
    farewell_active boolean DEFAULT false NOT NULL,
    farewell_message character varying,
    agent_lights text,
    client_lights text,
    account_id integer,
    page_id character varying,
    page_name character varying,
    page_token character varying,
    channel_type integer,
    sync_date timestamp without time zone,
    tagging_active boolean DEFAULT true,
    picture character varying,
    channel_name character varying,
    priority integer DEFAULT 3,
    read_only boolean DEFAULT false,
    ticket_mode integer,
    tagging_mode integer,
    source_filters character varying DEFAULT '111'::character varying,
    create_contacts boolean DEFAULT true
);


ALTER TABLE campaign OWNER TO postgres;

--
-- TOC entry 193 (class 1259 OID 17428)
-- Name: campaign_campaign_id_seq; Type: SEQUENCE; Schema: xxxx; Owner: postgres
--

CREATE SEQUENCE campaign_campaign_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE campaign_campaign_id_seq OWNER TO postgres;

--
-- TOC entry 3287 (class 0 OID 0)
-- Dependencies: 193
-- Name: campaign_campaign_id_seq; Type: SEQUENCE OWNED BY; Schema: xxxx; Owner: postgres
--

ALTER SEQUENCE campaign_campaign_id_seq OWNED BY campaign.campaign_id;


--
-- TOC entry 194 (class 1259 OID 17430)
-- Name: campaign_group; Type: TABLE; Schema: xxxx; Owner: postgres
--

CREATE TABLE campaign_group (
    campaign_group_id integer NOT NULL,
    campaign_id integer NOT NULL,
    group_id integer NOT NULL
);


ALTER TABLE campaign_group OWNER TO postgres;

--
-- TOC entry 195 (class 1259 OID 17433)
-- Name: campaign_group_campaign_group_id_seq; Type: SEQUENCE; Schema: xxxx; Owner: postgres
--

CREATE SEQUENCE campaign_group_campaign_group_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE campaign_group_campaign_group_id_seq OWNER TO postgres;

--
-- TOC entry 3288 (class 0 OID 0)
-- Dependencies: 195
-- Name: campaign_group_campaign_group_id_seq; Type: SEQUENCE OWNED BY; Schema: xxxx; Owner: postgres
--

ALTER SEQUENCE campaign_group_campaign_group_id_seq OWNED BY campaign_group.campaign_group_id;


--
-- TOC entry 196 (class 1259 OID 17435)
-- Name: campaign_script; Type: TABLE; Schema: xxxx; Owner: postgres
--

CREATE TABLE campaign_script (
    campaign_script_id integer NOT NULL,
    campaign_id integer NOT NULL,
    name character varying NOT NULL,
    value text NOT NULL
);


ALTER TABLE campaign_script OWNER TO postgres;

--
-- TOC entry 197 (class 1259 OID 17441)
-- Name: campaign_script_campaign_script_id_seq; Type: SEQUENCE; Schema: xxxx; Owner: postgres
--

CREATE SEQUENCE campaign_script_campaign_script_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE campaign_script_campaign_script_id_seq OWNER TO postgres;

--
-- TOC entry 3289 (class 0 OID 0)
-- Dependencies: 197
-- Name: campaign_script_campaign_script_id_seq; Type: SEQUENCE OWNED BY; Schema: xxxx; Owner: postgres
--

ALTER SEQUENCE campaign_script_campaign_script_id_seq OWNED BY campaign_script.campaign_script_id;


--
-- TOC entry 198 (class 1259 OID 17443)
-- Name: campaign_tagging_result; Type: TABLE; Schema: xxxx; Owner: postgres
--

CREATE TABLE campaign_tagging_result (
    campaign_tagging_result_id integer NOT NULL,
    campaign_id integer,
    tagging_result_id integer
);


ALTER TABLE campaign_tagging_result OWNER TO postgres;

--
-- TOC entry 199 (class 1259 OID 17446)
-- Name: campaign_tagging_result_campaign_tagging_result_id_seq; Type: SEQUENCE; Schema: xxxx; Owner: postgres
--

CREATE SEQUENCE campaign_tagging_result_campaign_tagging_result_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE campaign_tagging_result_campaign_tagging_result_id_seq OWNER TO postgres;

--
-- TOC entry 3290 (class 0 OID 0)
-- Dependencies: 199
-- Name: campaign_tagging_result_campaign_tagging_result_id_seq; Type: SEQUENCE OWNED BY; Schema: xxxx; Owner: postgres
--

ALTER SEQUENCE campaign_tagging_result_campaign_tagging_result_id_seq OWNED BY campaign_tagging_result.campaign_tagging_result_id;


--
-- TOC entry 200 (class 1259 OID 17448)
-- Name: campaign_type; Type: TABLE; Schema: xxxx; Owner: postgres
--

CREATE TABLE campaign_type (
    campaign_type_id character varying(3) DEFAULT NULL::character varying NOT NULL,
    name character varying(45) DEFAULT NULL::character varying NOT NULL
);


ALTER TABLE campaign_type OWNER TO postgres;
--
-- TOC entry 201 (class 1259 OID 17453)
-- Name: cdr_cdr_id_seq; Type: SEQUENCE; Schema: xxxx; Owner: postgres
--

CREATE SEQUENCE cdr_cdr_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE cdr_cdr_id_seq OWNER TO postgres;

--
-- TOC entry 3291 (class 0 OID 0)
-- Dependencies: 201
-- Name: cdr_cdr_id_seq; Type: SEQUENCE OWNED BY; Schema: xxxx; Owner: postgres
--

ALTER SEQUENCE cdr_cdr_id_seq OWNED BY cdr.cdr_id;


--
-- TOC entry 202 (class 1259 OID 17455)
-- Name: cdr_tagging; Type: TABLE; Schema: xxxx; Owner: postgres
--

CREATE TABLE cdr_tagging (
    uniqueid character varying NOT NULL,
    tagging_id integer,
    ticket_id integer
);


ALTER TABLE cdr_tagging OWNER TO postgres;

--
-- TOC entry 203 (class 1259 OID 17461)
-- Name: cgroup; Type: TABLE; Schema: xxxx; Owner: postgres
--

CREATE TABLE cgroup (
    group_id integer NOT NULL,
    name character varying NOT NULL,
    deleted boolean DEFAULT false,
    answer_type integer DEFAULT 1
);


ALTER TABLE cgroup OWNER TO postgres;

--
-- TOC entry 204 (class 1259 OID 17469)
-- Name: cgroup_group_id_seq; Type: SEQUENCE; Schema: xxxx; Owner: postgres
--

CREATE SEQUENCE cgroup_group_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE cgroup_group_id_seq OWNER TO postgres;

--
-- TOC entry 3292 (class 0 OID 0)
-- Dependencies: 204
-- Name: cgroup_group_id_seq; Type: SEQUENCE OWNED BY; Schema: xxxx; Owner: postgres
--

ALTER SEQUENCE cgroup_group_id_seq OWNED BY cgroup.group_id;
--
-- TOC entry 207 (class 1259 OID 17484)
-- Name: configuration; Type: TABLE; Schema: xxxx; Owner: postgres
--

CREATE TABLE configuration (
    configuration_id smallint NOT NULL,
    preferences character varying
);


ALTER TABLE configuration OWNER TO postgres;


--
-- TOC entry 208 (class 1259 OID 17490)
-- Name: contact; Type: TABLE; Schema: xxxx; Owner: postgres
--

CREATE TABLE contact (
    name character varying NOT NULL,
    phone character varying,
    email character varying,
    company_name character varying,
    deleted boolean,
    contact_id integer NOT NULL
);


ALTER TABLE contact OWNER TO postgres;

--
-- TOC entry 209 (class 1259 OID 17496)
-- Name: contact_contact_id_seq; Type: SEQUENCE; Schema: xxxx; Owner: postgres
--

CREATE SEQUENCE contact_contact_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE contact_contact_id_seq OWNER TO postgres;

--
-- TOC entry 3294 (class 0 OID 0)
-- Dependencies: 209
-- Name: contact_contact_id_seq; Type: SEQUENCE OWNED BY; Schema: xxxx; Owner: postgres
--

ALTER SEQUENCE contact_contact_id_seq OWNED BY contact.contact_id;


--
-- TOC entry 210 (class 1259 OID 17498)
-- Name: contact_method; Type: TABLE; Schema: xxxx; Owner: postgres
--

CREATE TABLE contact_method (
    contact_method_id integer NOT NULL,
    contact_id integer NOT NULL,
    value character varying,
    channel_type integer
);


ALTER TABLE contact_method OWNER TO postgres;

--
-- TOC entry 211 (class 1259 OID 17504)
-- Name: contact_method_contact_method_id_seq; Type: SEQUENCE; Schema: xxxx; Owner: postgres
--

CREATE SEQUENCE contact_method_contact_method_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE contact_method_contact_method_id_seq OWNER TO postgres;

--
-- TOC entry 3295 (class 0 OID 0)
-- Dependencies: 211
-- Name: contact_method_contact_method_id_seq; Type: SEQUENCE OWNED BY; Schema: xxxx; Owner: postgres
--

ALTER SEQUENCE contact_method_contact_method_id_seq OWNED BY contact_method.contact_method_id;


--
-- TOC entry 212 (class 1259 OID 17506)
-- Name: conversation; Type: TABLE; Schema: xxxx; Owner: postgres
--

CREATE TABLE conversation (
    conversation_id integer NOT NULL,
    title character varying,
    created_date date,
    owner_id integer,
    type_id integer,
    from_id integer,
    to_id integer,
    deleted boolean DEFAULT false,
    last_message_date timestamp(0) without time zone,
    last_message character varying
);


ALTER TABLE conversation OWNER TO postgres;

--
-- TOC entry 213 (class 1259 OID 17513)
-- Name: conversation_attachment; Type: TABLE; Schema: xxxx; Owner: postgres
--

CREATE TABLE conversation_attachment (
    conversation_attachment_id integer NOT NULL,
    conversation_message_id integer NOT NULL,
    file_name character varying,
    file_type character varying,
    url character varying
);


ALTER TABLE conversation_attachment OWNER TO postgres;

--
-- TOC entry 214 (class 1259 OID 17519)
-- Name: conversation_attachment_conversation_attachment_id_seq; Type: SEQUENCE; Schema: xxxx; Owner: postgres
--

CREATE SEQUENCE conversation_attachment_conversation_attachment_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE conversation_attachment_conversation_attachment_id_seq OWNER TO postgres;

--
-- TOC entry 3296 (class 0 OID 0)
-- Dependencies: 214
-- Name: conversation_attachment_conversation_attachment_id_seq; Type: SEQUENCE OWNED BY; Schema: xxxx; Owner: postgres
--

ALTER SEQUENCE conversation_attachment_conversation_attachment_id_seq OWNED BY conversation_attachment.conversation_attachment_id;


--
-- TOC entry 215 (class 1259 OID 17521)
-- Name: conversation_conversation_id_seq; Type: SEQUENCE; Schema: xxxx; Owner: postgres
--

CREATE SEQUENCE conversation_conversation_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE conversation_conversation_id_seq OWNER TO postgres;

--
-- TOC entry 3297 (class 0 OID 0)
-- Dependencies: 215
-- Name: conversation_conversation_id_seq; Type: SEQUENCE OWNED BY; Schema: xxxx; Owner: postgres
--

ALTER SEQUENCE conversation_conversation_id_seq OWNED BY conversation.conversation_id;


--
-- TOC entry 216 (class 1259 OID 17523)
-- Name: conversation_member; Type: TABLE; Schema: xxxx; Owner: postgres
--

CREATE TABLE conversation_member (
    conversation_member_id integer NOT NULL,
    conversation_id integer NOT NULL,
    agent_id integer,
    unseen_items_date timestamp(0) without time zone
);


ALTER TABLE conversation_member OWNER TO postgres;

--
-- TOC entry 217 (class 1259 OID 17526)
-- Name: conversation_member_conversation_member_id_seq; Type: SEQUENCE; Schema: xxxx; Owner: postgres
--

CREATE SEQUENCE conversation_member_conversation_member_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE conversation_member_conversation_member_id_seq OWNER TO postgres;

--
-- TOC entry 3298 (class 0 OID 0)
-- Dependencies: 217
-- Name: conversation_member_conversation_member_id_seq; Type: SEQUENCE OWNED BY; Schema: xxxx; Owner: postgres
--

ALTER SEQUENCE conversation_member_conversation_member_id_seq OWNED BY conversation_member.conversation_member_id;


--
-- TOC entry 218 (class 1259 OID 17528)
-- Name: conversation_message; Type: TABLE; Schema: xxxx; Owner: postgres
--

CREATE TABLE conversation_message (
    conversation_message_id integer NOT NULL,
    conversation_id integer NOT NULL,
    message character varying,
    created_date timestamp without time zone,
    deleted boolean,
    from_id integer,
    has_attachments boolean
);


ALTER TABLE conversation_message OWNER TO postgres;

--
-- TOC entry 219 (class 1259 OID 17534)
-- Name: conversation_message_conversation_message_id_seq; Type: SEQUENCE; Schema: xxxx; Owner: postgres
--

CREATE SEQUENCE conversation_message_conversation_message_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE conversation_message_conversation_message_id_seq OWNER TO postgres;

--
-- TOC entry 3299 (class 0 OID 0)
-- Dependencies: 219
-- Name: conversation_message_conversation_message_id_seq; Type: SEQUENCE OWNED BY; Schema: xxxx; Owner: postgres
--

ALTER SEQUENCE conversation_message_conversation_message_id_seq OWNED BY conversation_message.conversation_message_id;


--
-- TOC entry 220 (class 1259 OID 17536)
-- Name: conversation_notification; Type: TABLE; Schema: xxxx; Owner: postgres
--

CREATE TABLE conversation_notification (
    notification_id integer NOT NULL,
    conversation_id integer NOT NULL,
    from_id integer NOT NULL,
    img_url character varying,
    message character varying,
    "time" timestamp without time zone,
    repeatable boolean DEFAULT false NOT NULL,
    deleted boolean DEFAULT false NOT NULL
);


ALTER TABLE conversation_notification OWNER TO postgres;

--
-- TOC entry 221 (class 1259 OID 17544)
-- Name: conversation_notification_notification_id_seq; Type: SEQUENCE; Schema: xxxx; Owner: postgres
--

CREATE SEQUENCE conversation_notification_notification_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE conversation_notification_notification_id_seq OWNER TO postgres;

--
-- TOC entry 3300 (class 0 OID 0)
-- Dependencies: 221
-- Name: conversation_notification_notification_id_seq; Type: SEQUENCE OWNED BY; Schema: xxxx; Owner: postgres
--

ALTER SEQUENCE conversation_notification_notification_id_seq OWNED BY conversation_notification.notification_id;


--
-- TOC entry 222 (class 1259 OID 17546)
-- Name: dialer; Type: TABLE; Schema: xxxx; Owner: postgres
--

CREATE TABLE dialer (
    dialer_id integer NOT NULL,
    name character varying,
    status integer,
    campaign_id integer,
    created_date timestamp without time zone,
    deleted boolean,
    current_position integer DEFAULT 0,
    total_entries integer,
    round integer DEFAULT 0,
    total_calls integer DEFAULT 0,
    total_closed integer DEFAULT 0
);


ALTER TABLE dialer OWNER TO postgres;

--
-- TOC entry 223 (class 1259 OID 17556)
-- Name: dialer_dialer_id_seq; Type: SEQUENCE; Schema: xxxx; Owner: postgres
--

CREATE SEQUENCE dialer_dialer_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE dialer_dialer_id_seq OWNER TO postgres;

--
-- TOC entry 3301 (class 0 OID 0)
-- Dependencies: 223
-- Name: dialer_dialer_id_seq; Type: SEQUENCE OWNED BY; Schema: xxxx; Owner: postgres
--

ALTER SEQUENCE dialer_dialer_id_seq OWNED BY dialer.dialer_id;


--
-- TOC entry 224 (class 1259 OID 17558)
-- Name: dialer_entry; Type: TABLE; Schema: xxxx; Owner: postgres
--

CREATE TABLE dialer_entry (
    dialer_entry_id integer NOT NULL,
    number character varying,
    name character varying,
    status integer,
    campaign_id integer,
    extra_fields json,
    dialer_id integer,
    "position" integer
);


ALTER TABLE dialer_entry OWNER TO postgres;

--
-- TOC entry 225 (class 1259 OID 17564)
-- Name: dialer_entry_dialer_entry_id_seq; Type: SEQUENCE; Schema: xxxx; Owner: postgres
--

CREATE SEQUENCE dialer_entry_dialer_entry_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE dialer_entry_dialer_entry_id_seq OWNER TO postgres;

--
-- TOC entry 3302 (class 0 OID 0)
-- Dependencies: 225
-- Name: dialer_entry_dialer_entry_id_seq; Type: SEQUENCE OWNED BY; Schema: xxxx; Owner: postgres
--

ALTER SEQUENCE dialer_entry_dialer_entry_id_seq OWNED BY dialer_entry.dialer_entry_id;


--
-- TOC entry 226 (class 1259 OID 17566)
-- Name: label; Type: TABLE; Schema: xxxx; Owner: postgres
--

CREATE TABLE label (
    label_id integer NOT NULL,
    color character varying,
    name character varying
);


ALTER TABLE label OWNER TO postgres;

--
-- TOC entry 227 (class 1259 OID 17572)
-- Name: label_label_id_seq; Type: SEQUENCE; Schema: xxxx; Owner: postgres
--

CREATE SEQUENCE label_label_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE label_label_id_seq OWNER TO postgres;

--
-- TOC entry 3303 (class 0 OID 0)
-- Dependencies: 227
-- Name: label_label_id_seq; Type: SEQUENCE OWNED BY; Schema: xxxx; Owner: postgres
--

ALTER SEQUENCE label_label_id_seq OWNED BY label.label_id;


--
-- TOC entry 228 (class 1259 OID 17574)
-- Name: notification; Type: TABLE; Schema: xxxx; Owner: postgres
--

CREATE TABLE notification (
    notification_id integer NOT NULL,
    from_id integer NOT NULL,
    img_url character varying,
    message character varying,
    "time" timestamp without time zone,
    repeatable boolean DEFAULT false NOT NULL,
    deleted boolean DEFAULT false NOT NULL
);


ALTER TABLE notification OWNER TO postgres;

--
-- TOC entry 229 (class 1259 OID 17582)
-- Name: notification_campaign; Type: TABLE; Schema: xxxx; Owner: postgres
--

CREATE TABLE notification_campaign (
    notification_campaign_id integer NOT NULL,
    conversation_id integer NOT NULL,
    notification_id integer NOT NULL
);


ALTER TABLE notification_campaign OWNER TO postgres;

--
-- TOC entry 230 (class 1259 OID 17585)
-- Name: notification_campaign_notification_campaign_id_seq; Type: SEQUENCE; Schema: xxxx; Owner: postgres
--

CREATE SEQUENCE notification_campaign_notification_campaign_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE notification_campaign_notification_campaign_id_seq OWNER TO postgres;

--
-- TOC entry 3304 (class 0 OID 0)
-- Dependencies: 230
-- Name: notification_campaign_notification_campaign_id_seq; Type: SEQUENCE OWNED BY; Schema: xxxx; Owner: postgres
--

ALTER SEQUENCE notification_campaign_notification_campaign_id_seq OWNED BY notification_campaign.notification_campaign_id;


--
-- TOC entry 231 (class 1259 OID 17587)
-- Name: notification_notification_id_seq; Type: SEQUENCE; Schema: xxxx; Owner: postgres
--

CREATE SEQUENCE notification_notification_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE notification_notification_id_seq OWNER TO postgres;

--
-- TOC entry 3305 (class 0 OID 0)
-- Dependencies: 231
-- Name: notification_notification_id_seq; Type: SEQUENCE OWNED BY; Schema: xxxx; Owner: postgres
--

ALTER SEQUENCE notification_notification_id_seq OWNED BY notification.notification_id;


--
-- TOC entry 232 (class 1259 OID 17589)
-- Name: queue_disposition; Type: VIEW; Schema: xxxx; Owner: postgres
--

CREATE VIEW queue_disposition AS
 SELECT queue_log.callid AS uniqueid,
    queue_log.queuename,
    queue_log.event AS disposition,
    queue_log.created,
        CASE
            WHEN ((queue_log.event)::text = 'ABANDON'::text) THEN 'ABANDON'::text
            ELSE 'ANSWERED'::text
        END AS answered,
        CASE
            WHEN ((queue_log.event)::text = ANY (ARRAY[('ABANDON'::character varying)::text, ('ATTENDEDTRANSFER'::character varying)::text, ('BLINDTRANSFER'::character varying)::text])) THEN queue_log.data3
            ELSE queue_log.data1
        END AS hold_duration,
        CASE
            WHEN ((queue_log.event)::text = ANY (ARRAY[('ATTENDEDTRANSFER'::character varying)::text, ('BLINDTRANSFER'::character varying)::text])) THEN queue_log.data4
            WHEN ((queue_log.event)::text = 'ABANDON'::text) THEN '0'::character varying
            ELSE queue_log.data2
        END AS talk_duration
   FROM (queue_log
     JOIN ( SELECT queue_log_1.callid,
            max(queue_log_1.created) AS created
           FROM queue_log queue_log_1
          GROUP BY queue_log_1.callid) ql1 USING (callid, created))
  WHERE ((queue_log.event)::text = ANY (ARRAY[('ABANDON'::character varying)::text, ('COMPLETECALLER'::character varying)::text, ('COMPLETEAGENT'::character varying)::text, ('ATTENDEDTRANSFER'::character varying)::text, ('BLINDTRANSFER'::character varying)::text]));


ALTER TABLE queue_disposition OWNER TO postgres;

--
-- TOC entry 233 (class 1259 OID 17594)
-- Name: queue_log_queue_log_id_seq; Type: SEQUENCE; Schema: xxxx; Owner: postgres
--

CREATE SEQUENCE queue_log_queue_log_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE queue_log_queue_log_id_seq OWNER TO postgres;

--
-- TOC entry 3306 (class 0 OID 0)
-- Dependencies: 233
-- Name: queue_log_queue_log_id_seq; Type: SEQUENCE OWNED BY; Schema: xxxx; Owner: postgres
--

ALTER SEQUENCE queue_log_queue_log_id_seq OWNED BY queue_log.queue_log_id;


--
-- TOC entry 234 (class 1259 OID 17596)
-- Name: report_content; Type: TABLE; Schema: xxxx; Owner: postgres
--

CREATE TABLE report_content (
    report_content_id integer NOT NULL,
    report_event_id integer NOT NULL,
    report_type integer
);


ALTER TABLE report_content OWNER TO postgres;

--
-- TOC entry 235 (class 1259 OID 17599)
-- Name: report_content_report_content_id_seq; Type: SEQUENCE; Schema: xxxx; Owner: postgres
--

CREATE SEQUENCE report_content_report_content_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE report_content_report_content_id_seq OWNER TO postgres;

--
-- TOC entry 3307 (class 0 OID 0)
-- Dependencies: 235
-- Name: report_content_report_content_id_seq; Type: SEQUENCE OWNED BY; Schema: xxxx; Owner: postgres
--

ALTER SEQUENCE report_content_report_content_id_seq OWNED BY report_content.report_content_id;


--
-- TOC entry 236 (class 1259 OID 17601)
-- Name: report_destination; Type: TABLE; Schema: xxxx; Owner: postgres
--

CREATE TABLE report_destination (
    report_destination_id integer NOT NULL,
    report_event_id integer NOT NULL,
    destination character varying NOT NULL
);


ALTER TABLE report_destination OWNER TO postgres;

--
-- TOC entry 237 (class 1259 OID 17607)
-- Name: report_destination_report_destination_id_seq; Type: SEQUENCE; Schema: xxxx; Owner: postgres
--

CREATE SEQUENCE report_destination_report_destination_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE report_destination_report_destination_id_seq OWNER TO postgres;

--
-- TOC entry 3308 (class 0 OID 0)
-- Dependencies: 237
-- Name: report_destination_report_destination_id_seq; Type: SEQUENCE OWNED BY; Schema: xxxx; Owner: postgres
--

ALTER SEQUENCE report_destination_report_destination_id_seq OWNED BY report_destination.report_destination_id;


--
-- TOC entry 238 (class 1259 OID 17609)
-- Name: report_event; Type: TABLE; Schema: xxxx; Owner: postgres
--

CREATE TABLE report_event (
    report_event_id integer NOT NULL,
    name character varying,
    group_id integer,
    "time" timestamp without time zone DEFAULT ('now'::text)::timestamp(6) with time zone,
    timezone integer DEFAULT 0
);


ALTER TABLE report_event OWNER TO postgres;

--
-- TOC entry 239 (class 1259 OID 17617)
-- Name: report_event_report_event_id_seq; Type: SEQUENCE; Schema: xxxx; Owner: postgres
--

CREATE SEQUENCE report_event_report_event_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE report_event_report_event_id_seq OWNER TO postgres;

--
-- TOC entry 3309 (class 0 OID 0)
-- Dependencies: 239
-- Name: report_event_report_event_id_seq; Type: SEQUENCE OWNED BY; Schema: xxxx; Owner: postgres
--

ALTER SEQUENCE report_event_report_event_id_seq OWNED BY report_event.report_event_id;


--
-- TOC entry 240 (class 1259 OID 17619)
-- Name: report_log; Type: TABLE; Schema: xxxx; Owner: postgres
--

CREATE TABLE report_log (
    report_log_id integer NOT NULL,
    report_event_id integer NOT NULL,
    day timestamp without time zone DEFAULT ('now'::text)::timestamp(6) with time zone,
    result character varying NOT NULL
);


ALTER TABLE report_log OWNER TO postgres;

--
-- TOC entry 241 (class 1259 OID 17626)
-- Name: report_log_report_log_id_seq; Type: SEQUENCE; Schema: xxxx; Owner: postgres
--

CREATE SEQUENCE report_log_report_log_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE report_log_report_log_id_seq OWNER TO postgres;

--
-- TOC entry 3310 (class 0 OID 0)
-- Dependencies: 241
-- Name: report_log_report_log_id_seq; Type: SEQUENCE OWNED BY; Schema: xxxx; Owner: postgres
--

ALTER SEQUENCE report_log_report_log_id_seq OWNED BY report_log.report_log_id;


--
-- TOC entry 242 (class 1259 OID 17628)
-- Name: status_code; Type: TABLE; Schema: xxxx; Owner: postgres
--

CREATE TABLE status_code (
    status_code_id character varying(3) DEFAULT NULL::character varying NOT NULL,
    name character varying(45) DEFAULT NULL::character varying NOT NULL,
    billable boolean DEFAULT false,
    deleted boolean DEFAULT false
);


ALTER TABLE status_code OWNER TO postgres;


--
-- TOC entry 243 (class 1259 OID 17635)
-- Name: tagging; Type: TABLE; Schema: xxxx; Owner: postgres
--

CREATE TABLE tagging (
    tagging_id bigint NOT NULL,
    notes character varying(1000) DEFAULT NULL::character varying,
    datetime timestamp(6) without time zone DEFAULT NULL::timestamp without time zone NOT NULL,
    agent_id integer NOT NULL,
    tagging_result_id integer,
    campaign_id integer,
    transferred boolean DEFAULT false,
    transferred_agents character varying
);


ALTER TABLE tagging OWNER TO postgres;

--
-- TOC entry 244 (class 1259 OID 17644)
-- Name: tagging_result; Type: TABLE; Schema: xxxx; Owner: postgres
--

CREATE TABLE tagging_result (
    tagging_result_id integer NOT NULL,
    name character varying(100) DEFAULT NULL::character varying NOT NULL,
    deleted boolean DEFAULT false NOT NULL,
    parent_id integer,
    dont_call boolean DEFAULT false
);


ALTER TABLE tagging_result OWNER TO postgres;

--
-- TOC entry 245 (class 1259 OID 17650)
-- Name: tagging_result_tagging_result_id_seq; Type: SEQUENCE; Schema: xxxx; Owner: postgres
--

CREATE SEQUENCE tagging_result_tagging_result_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tagging_result_tagging_result_id_seq OWNER TO postgres;

--
-- TOC entry 3311 (class 0 OID 0)
-- Dependencies: 245
-- Name: tagging_result_tagging_result_id_seq; Type: SEQUENCE OWNED BY; Schema: xxxx; Owner: postgres
--

ALTER SEQUENCE tagging_result_tagging_result_id_seq OWNED BY tagging_result.tagging_result_id;


--
-- TOC entry 246 (class 1259 OID 17652)
-- Name: tagging_tagging_id_seq; Type: SEQUENCE; Schema: xxxx; Owner: postgres
--

CREATE SEQUENCE tagging_tagging_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tagging_tagging_id_seq OWNER TO postgres;

--
-- TOC entry 3312 (class 0 OID 0)
-- Dependencies: 246
-- Name: tagging_tagging_id_seq; Type: SEQUENCE OWNED BY; Schema: xxxx; Owner: postgres
--

ALTER SEQUENCE tagging_tagging_id_seq OWNED BY tagging.tagging_id;


--
-- TOC entry 247 (class 1259 OID 17654)
-- Name: ticket; Type: TABLE; Schema: xxxx; Owner: postgres
--

CREATE TABLE ticket (
    ticket_id integer NOT NULL,
    campaign_id integer,
    agent_id integer,
    picture character varying,
    title character varying NOT NULL,
    from_name character varying,
    source_type integer NOT NULL,
    like_count integer,
    created_date timestamp without time zone DEFAULT ('now'::text)::timestamp(6) with time zone NOT NULL,
    ticket_status integer NOT NULL,
    from_id character varying,
    last_message character varying,
    last_message_date timestamp(6) without time zone,
    source_id character varying,
    source_url character varying,
    reopened_date date,
    closed_date timestamp without time zone,
    updated_date date,
    source_deleted boolean,
    channel_type integer,
    is_public boolean,
    unseen_items integer,
    ticket_parent integer,
    cc character varying,
    email character varying,
    end_date date,
    contact_id integer,
    tagging_result_id integer,
    notes character varying(1000),
    transferred boolean,
    transferred_agents character varying,
    external_ticket_id integer,
    duration integer,
    mask_count integer,
    mask_times character varying,
    extra_fields character varying DEFAULT ''::character varying,
    priority integer DEFAULT 3,
    labels oid,
    attended_date timestamp without time zone
);


ALTER TABLE ticket OWNER TO postgres;

--
-- TOC entry 248 (class 1259 OID 17663)
-- Name: ticket_asignee; Type: TABLE; Schema: xxxx; Owner: postgres
--

CREATE TABLE ticket_asignee (
    ticket_asignee_id integer NOT NULL,
    ticket_id integer NOT NULL,
    agent_id integer NOT NULL,
    asignee_date timestamp without time zone
);


ALTER TABLE ticket_asignee OWNER TO postgres;

--
-- TOC entry 249 (class 1259 OID 17666)
-- Name: ticket_asignee_ticket_asignee_id_seq; Type: SEQUENCE; Schema: xxxx; Owner: postgres
--

CREATE SEQUENCE ticket_asignee_ticket_asignee_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE ticket_asignee_ticket_asignee_id_seq OWNER TO postgres;

--
-- TOC entry 3313 (class 0 OID 0)
-- Dependencies: 249
-- Name: ticket_asignee_ticket_asignee_id_seq; Type: SEQUENCE OWNED BY; Schema: xxxx; Owner: postgres
--

ALTER SEQUENCE ticket_asignee_ticket_asignee_id_seq OWNED BY ticket_asignee.ticket_asignee_id;


--
-- TOC entry 250 (class 1259 OID 17668)
-- Name: ticket_attachment; Type: TABLE; Schema: xxxx; Owner: postgres
--

CREATE TABLE ticket_attachment (
    ticket_attachment_id integer NOT NULL,
    ticket_message_id integer,
    url character varying,
    file_type character varying,
    file_name character varying
);


ALTER TABLE ticket_attachment OWNER TO postgres;

--
-- TOC entry 251 (class 1259 OID 17674)
-- Name: ticket_attachment_ticket_attachment_id_seq; Type: SEQUENCE; Schema: xxxx; Owner: postgres
--

CREATE SEQUENCE ticket_attachment_ticket_attachment_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE ticket_attachment_ticket_attachment_id_seq OWNER TO postgres;

--
-- TOC entry 3314 (class 0 OID 0)
-- Dependencies: 251
-- Name: ticket_attachment_ticket_attachment_id_seq; Type: SEQUENCE OWNED BY; Schema: xxxx; Owner: postgres
--

ALTER SEQUENCE ticket_attachment_ticket_attachment_id_seq OWNED BY ticket_attachment.ticket_attachment_id;


--
-- TOC entry 452 (class 1259 OID 65546)
-- Name: ticket_label; Type: TABLE; Schema: xxxx; Owner: postgres
--

CREATE TABLE ticket_label (
    ticket_label_id integer NOT NULL,
    ticket_id integer,
    label_id integer
);


ALTER TABLE ticket_label OWNER TO postgres;

--
-- TOC entry 451 (class 1259 OID 65544)
-- Name: ticket_label_ticket_label_id_seq; Type: SEQUENCE; Schema: xxxx; Owner: postgres
--

CREATE SEQUENCE ticket_label_ticket_label_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE ticket_label_ticket_label_id_seq OWNER TO postgres;

--
-- TOC entry 3315 (class 0 OID 0)
-- Dependencies: 451
-- Name: ticket_label_ticket_label_id_seq; Type: SEQUENCE OWNED BY; Schema: xxxx; Owner: postgres
--

ALTER SEQUENCE ticket_label_ticket_label_id_seq OWNED BY ticket_label.ticket_label_id;


--
-- TOC entry 252 (class 1259 OID 17681)
-- Name: ticket_log; Type: TABLE; Schema: xxxx; Owner: postgres
--

CREATE TABLE ticket_log (
    ticket_log_id integer NOT NULL,
    ticket_id integer,
    agent_id integer,
    ticket_status integer NOT NULL,
    created_date timestamp without time zone DEFAULT ('now'::text)::timestamp(6) with time zone NOT NULL
);


ALTER TABLE ticket_log OWNER TO postgres;

--
-- TOC entry 253 (class 1259 OID 17685)
-- Name: ticket_log_ticket_log_id_seq; Type: SEQUENCE; Schema: xxxx; Owner: postgres
--

CREATE SEQUENCE ticket_log_ticket_log_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE ticket_log_ticket_log_id_seq OWNER TO postgres;

--
-- TOC entry 3316 (class 0 OID 0)
-- Dependencies: 253
-- Name: ticket_log_ticket_log_id_seq; Type: SEQUENCE OWNED BY; Schema: xxxx; Owner: postgres
--

ALTER SEQUENCE ticket_log_ticket_log_id_seq OWNED BY ticket_log.ticket_log_id;


--
-- TOC entry 254 (class 1259 OID 17687)
-- Name: ticket_message; Type: TABLE; Schema: xxxx; Owner: postgres
--

CREATE TABLE ticket_message (
    ticket_id integer,
    ticket_message_id integer NOT NULL,
    from_id character varying,
    "to" character varying,
    message character varying,
    created_date timestamp without time zone,
    internal boolean,
    deleted boolean,
    source_id character varying(255),
    agent_id integer,
    from_name character varying(255),
    picture character varying(255),
    by_agent boolean,
    public_subticket_id integer,
    private_subticket_id integer,
    has_attachments boolean,
    channel_type integer
);


ALTER TABLE ticket_message OWNER TO postgres;

--
-- TOC entry 255 (class 1259 OID 17693)
-- Name: ticket_message_ticket_message_id_seq; Type: SEQUENCE; Schema: xxxx; Owner: postgres
--

CREATE SEQUENCE ticket_message_ticket_message_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE ticket_message_ticket_message_id_seq OWNER TO postgres;

--
-- TOC entry 3317 (class 0 OID 0)
-- Dependencies: 255
-- Name: ticket_message_ticket_message_id_seq; Type: SEQUENCE OWNED BY; Schema: xxxx; Owner: postgres
--

ALTER SEQUENCE ticket_message_ticket_message_id_seq OWNED BY ticket_message.ticket_message_id;


--
-- TOC entry 256 (class 1259 OID 17695)
-- Name: ticket_script; Type: TABLE; Schema: xxxx; Owner: postgres
--

CREATE TABLE ticket_script (
    ticket_script_id integer NOT NULL,
    title character varying,
    text character varying,
    parent_id integer,
    deleted boolean,
    script_type integer
);


ALTER TABLE ticket_script OWNER TO postgres;

--
-- TOC entry 257 (class 1259 OID 17701)
-- Name: ticket_script_ticket_script_id_seq; Type: SEQUENCE; Schema: xxxx; Owner: postgres
--

CREATE SEQUENCE ticket_script_ticket_script_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE ticket_script_ticket_script_id_seq OWNER TO postgres;

--
-- TOC entry 3318 (class 0 OID 0)
-- Dependencies: 257
-- Name: ticket_script_ticket_script_id_seq; Type: SEQUENCE OWNED BY; Schema: xxxx; Owner: postgres
--

ALTER SEQUENCE ticket_script_ticket_script_id_seq OWNED BY ticket_script.ticket_script_id;


--
-- TOC entry 258 (class 1259 OID 17703)
-- Name: ticket_ticket_id_seq; Type: SEQUENCE; Schema: xxxx; Owner: postgres
--

CREATE SEQUENCE ticket_ticket_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE ticket_ticket_id_seq OWNER TO postgres;

--
-- TOC entry 3319 (class 0 OID 0)
-- Dependencies: 258
-- Name: ticket_ticket_id_seq; Type: SEQUENCE OWNED BY; Schema: xxxx; Owner: postgres
--

ALTER SEQUENCE ticket_ticket_id_seq OWNED BY ticket.ticket_id;


--
-- TOC entry 259 (class 1259 OID 17705)
-- Name: visitor; Type: TABLE; Schema: xxxx; Owner: postgres
--

CREATE TABLE visitor (
    visitor_id integer NOT NULL,
    visitor_uuid character varying(45) NOT NULL,
    location character varying,
    zip_code character varying,
    ip_address character varying,
    isp character varying,
    duration integer NOT NULL,
    created_date timestamp(6) without time zone DEFAULT NULL::timestamp without time zone NOT NULL,
    ended_date timestamp(6) without time zone DEFAULT NULL::timestamp without time zone NOT NULL
);


ALTER TABLE visitor OWNER TO postgres;

--
-- TOC entry 260 (class 1259 OID 17713)
-- Name: visitor_visitor_id_seq; Type: SEQUENCE; Schema: xxxx; Owner: postgres
--

CREATE SEQUENCE visitor_visitor_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE visitor_visitor_id_seq OWNER TO postgres;

--
-- TOC entry 3320 (class 0 OID 0)
-- Dependencies: 260
-- Name: visitor_visitor_id_seq; Type: SEQUENCE OWNED BY; Schema: xxxx; Owner: postgres
--

ALTER SEQUENCE visitor_visitor_id_seq OWNED BY visitor.visitor_id;


--
-- TOC entry 261 (class 1259 OID 17715)
-- Name: wall_message; Type: TABLE; Schema: xxxx; Owner: postgres
--

CREATE TABLE wall_message (
    wall_message_id bigint NOT NULL,
    message text NOT NULL,
    created_date timestamp(6) without time zone DEFAULT NULL::timestamp without time zone NOT NULL,
    agent_id integer NOT NULL,
    deleted boolean DEFAULT false,
    updated boolean DEFAULT false
);


ALTER TABLE wall_message OWNER TO postgres;

--
-- TOC entry 262 (class 1259 OID 17724)
-- Name: wall_message_wall_message_id_seq; Type: SEQUENCE; Schema: xxxx; Owner: postgres
--

CREATE SEQUENCE wall_message_wall_message_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE wall_message_wall_message_id_seq OWNER TO postgres;

--
-- TOC entry 3321 (class 0 OID 0)
-- Dependencies: 262
-- Name: wall_message_wall_message_id_seq; Type: SEQUENCE OWNED BY; Schema: xxxx; Owner: postgres
--

ALTER SEQUENCE wall_message_wall_message_id_seq OWNED BY wall_message.wall_message_id;


--
-- TOC entry 2798 (class 2604 OID 19854)
-- Name: account account_id; Type: DEFAULT; Schema: xxxx; Owner: postgres
--

ALTER TABLE ONLY account ALTER COLUMN account_id SET DEFAULT nextval('account_account_id_seq'::regclass);


--
-- TOC entry 2800 (class 2604 OID 19855)
-- Name: agent agent_id; Type: DEFAULT; Schema: xxxx; Owner: postgres
--

ALTER TABLE ONLY agent ALTER COLUMN agent_id SET DEFAULT nextval('agent_agent_id_seq'::regclass);


--
-- TOC entry 2806 (class 2604 OID 19856)
-- Name: agent_campaign agent_campaign_id; Type: DEFAULT; Schema: xxxx; Owner: postgres
--

ALTER TABLE ONLY agent_campaign ALTER COLUMN agent_campaign_id SET DEFAULT nextval('agent_campaign_agent_campaign_id_seq'::regclass);


--
-- TOC entry 2808 (class 2604 OID 19857)
-- Name: agent_status_history agent_status_history_id; Type: DEFAULT; Schema: xxxx; Owner: postgres
--

ALTER TABLE ONLY agent_status_history ALTER COLUMN agent_status_history_id SET DEFAULT nextval('agent_status_history_agent_status_history_id_seq'::regclass);


--
-- TOC entry 2815 (class 2604 OID 19858)
-- Name: campaign campaign_id; Type: DEFAULT; Schema: xxxx; Owner: postgres
--

ALTER TABLE ONLY campaign ALTER COLUMN campaign_id SET DEFAULT nextval('campaign_campaign_id_seq'::regclass);


--
-- TOC entry 2826 (class 2604 OID 19859)
-- Name: campaign_group campaign_group_id; Type: DEFAULT; Schema: xxxx; Owner: postgres
--

ALTER TABLE ONLY campaign_group ALTER COLUMN campaign_group_id SET DEFAULT nextval('campaign_group_campaign_group_id_seq'::regclass);


--
-- TOC entry 2827 (class 2604 OID 19860)
-- Name: campaign_script campaign_script_id; Type: DEFAULT; Schema: xxxx; Owner: postgres
--

ALTER TABLE ONLY campaign_script ALTER COLUMN campaign_script_id SET DEFAULT nextval('campaign_script_campaign_script_id_seq'::regclass);


--
-- TOC entry 2828 (class 2604 OID 19861)
-- Name: campaign_tagging_result campaign_tagging_result_id; Type: DEFAULT; Schema: xxxx; Owner: postgres
--

ALTER TABLE ONLY campaign_tagging_result ALTER COLUMN campaign_tagging_result_id SET DEFAULT nextval('campaign_tagging_result_campaign_tagging_result_id_seq'::regclass);


--
-- TOC entry 2811 (class 2604 OID 19862)
-- Name: cdr cdr_id; Type: DEFAULT; Schema: xxxx; Owner: postgres
--

ALTER TABLE ONLY cdr ALTER COLUMN cdr_id SET DEFAULT nextval('cdr_cdr_id_seq'::regclass);


--
-- TOC entry 2831 (class 2604 OID 19863)
-- Name: cgroup group_id; Type: DEFAULT; Schema: xxxx; Owner: postgres
--

ALTER TABLE ONLY cgroup ALTER COLUMN group_id SET DEFAULT nextval('cgroup_group_id_seq'::regclass);

--
-- TOC entry 2840 (class 2604 OID 19865)
-- Name: contact contact_id; Type: DEFAULT; Schema: xxxx; Owner: postgres
--

ALTER TABLE ONLY contact ALTER COLUMN contact_id SET DEFAULT nextval('contact_contact_id_seq'::regclass);


--
-- TOC entry 2841 (class 2604 OID 19866)
-- Name: contact_method contact_method_id; Type: DEFAULT; Schema: xxxx; Owner: postgres
--

ALTER TABLE ONLY contact_method ALTER COLUMN contact_method_id SET DEFAULT nextval('contact_method_contact_method_id_seq'::regclass);


--
-- TOC entry 2842 (class 2604 OID 19867)
-- Name: conversation conversation_id; Type: DEFAULT; Schema: xxxx; Owner: postgres
--

ALTER TABLE ONLY conversation ALTER COLUMN conversation_id SET DEFAULT nextval('conversation_conversation_id_seq'::regclass);


--
-- TOC entry 2844 (class 2604 OID 19868)
-- Name: conversation_attachment conversation_attachment_id; Type: DEFAULT; Schema: xxxx; Owner: postgres
--

ALTER TABLE ONLY conversation_attachment ALTER COLUMN conversation_attachment_id SET DEFAULT nextval('conversation_attachment_conversation_attachment_id_seq'::regclass);


--
-- TOC entry 2845 (class 2604 OID 19869)
-- Name: conversation_member conversation_member_id; Type: DEFAULT; Schema: xxxx; Owner: postgres
--

ALTER TABLE ONLY conversation_member ALTER COLUMN conversation_member_id SET DEFAULT nextval('conversation_member_conversation_member_id_seq'::regclass);


--
-- TOC entry 2846 (class 2604 OID 19870)
-- Name: conversation_message conversation_message_id; Type: DEFAULT; Schema: xxxx; Owner: postgres
--

ALTER TABLE ONLY conversation_message ALTER COLUMN conversation_message_id SET DEFAULT nextval('conversation_message_conversation_message_id_seq'::regclass);


--
-- TOC entry 2847 (class 2604 OID 19871)
-- Name: conversation_notification notification_id; Type: DEFAULT; Schema: xxxx; Owner: postgres
--

ALTER TABLE ONLY conversation_notification ALTER COLUMN notification_id SET DEFAULT nextval('conversation_notification_notification_id_seq'::regclass);


--
-- TOC entry 2850 (class 2604 OID 19872)
-- Name: dialer dialer_id; Type: DEFAULT; Schema: xxxx; Owner: postgres
--

ALTER TABLE ONLY dialer ALTER COLUMN dialer_id SET DEFAULT nextval('dialer_dialer_id_seq'::regclass);


--
-- TOC entry 2855 (class 2604 OID 19873)
-- Name: dialer_entry dialer_entry_id; Type: DEFAULT; Schema: xxxx; Owner: postgres
--

ALTER TABLE ONLY dialer_entry ALTER COLUMN dialer_entry_id SET DEFAULT nextval('dialer_entry_dialer_entry_id_seq'::regclass);


--
-- TOC entry 2856 (class 2604 OID 19874)
-- Name: label label_id; Type: DEFAULT; Schema: xxxx; Owner: postgres
--

ALTER TABLE ONLY label ALTER COLUMN label_id SET DEFAULT nextval('label_label_id_seq'::regclass);


--
-- TOC entry 2857 (class 2604 OID 19875)
-- Name: notification notification_id; Type: DEFAULT; Schema: xxxx; Owner: postgres
--

ALTER TABLE ONLY notification ALTER COLUMN notification_id SET DEFAULT nextval('notification_notification_id_seq'::regclass);


--
-- TOC entry 2860 (class 2604 OID 19876)
-- Name: notification_campaign notification_campaign_id; Type: DEFAULT; Schema: xxxx; Owner: postgres
--

ALTER TABLE ONLY notification_campaign ALTER COLUMN notification_campaign_id SET DEFAULT nextval('notification_campaign_notification_campaign_id_seq'::regclass);


--
-- TOC entry 2812 (class 2604 OID 19877)
-- Name: queue_log queue_log_id; Type: DEFAULT; Schema: xxxx; Owner: postgres
--

ALTER TABLE ONLY queue_log ALTER COLUMN queue_log_id SET DEFAULT nextval('queue_log_queue_log_id_seq'::regclass);


--
-- TOC entry 2861 (class 2604 OID 19878)
-- Name: report_content report_content_id; Type: DEFAULT; Schema: xxxx; Owner: postgres
--

ALTER TABLE ONLY report_content ALTER COLUMN report_content_id SET DEFAULT nextval('report_content_report_content_id_seq'::regclass);


--
-- TOC entry 2862 (class 2604 OID 19879)
-- Name: report_destination report_destination_id; Type: DEFAULT; Schema: xxxx; Owner: postgres
--

ALTER TABLE ONLY report_destination ALTER COLUMN report_destination_id SET DEFAULT nextval('report_destination_report_destination_id_seq'::regclass);


--
-- TOC entry 2863 (class 2604 OID 19880)
-- Name: report_event report_event_id; Type: DEFAULT; Schema: xxxx; Owner: postgres
--

ALTER TABLE ONLY report_event ALTER COLUMN report_event_id SET DEFAULT nextval('report_event_report_event_id_seq'::regclass);


--
-- TOC entry 2866 (class 2604 OID 19881)
-- Name: report_log report_log_id; Type: DEFAULT; Schema: xxxx; Owner: postgres
--

ALTER TABLE ONLY report_log ALTER COLUMN report_log_id SET DEFAULT nextval('report_log_report_log_id_seq'::regclass);


--
-- TOC entry 2872 (class 2604 OID 19882)
-- Name: tagging tagging_id; Type: DEFAULT; Schema: xxxx; Owner: postgres
--

ALTER TABLE ONLY tagging ALTER COLUMN tagging_id SET DEFAULT nextval('tagging_tagging_id_seq'::regclass);


--
-- TOC entry 2876 (class 2604 OID 19883)
-- Name: tagging_result tagging_result_id; Type: DEFAULT; Schema: xxxx; Owner: postgres
--

ALTER TABLE ONLY tagging_result ALTER COLUMN tagging_result_id SET DEFAULT nextval('tagging_result_tagging_result_id_seq'::regclass);


--
-- TOC entry 2880 (class 2604 OID 19884)
-- Name: ticket ticket_id; Type: DEFAULT; Schema: xxxx; Owner: postgres
--

ALTER TABLE ONLY ticket ALTER COLUMN ticket_id SET DEFAULT nextval('ticket_ticket_id_seq'::regclass);


--
-- TOC entry 2884 (class 2604 OID 19885)
-- Name: ticket_asignee ticket_asignee_id; Type: DEFAULT; Schema: xxxx; Owner: postgres
--

ALTER TABLE ONLY ticket_asignee ALTER COLUMN ticket_asignee_id SET DEFAULT nextval('ticket_asignee_ticket_asignee_id_seq'::regclass);


--
-- TOC entry 2885 (class 2604 OID 19886)
-- Name: ticket_attachment ticket_attachment_id; Type: DEFAULT; Schema: xxxx; Owner: postgres
--

ALTER TABLE ONLY ticket_attachment ALTER COLUMN ticket_attachment_id SET DEFAULT nextval('ticket_attachment_ticket_attachment_id_seq'::regclass);


--
-- TOC entry 2897 (class 2604 OID 65549)
-- Name: ticket_label ticket_label_id; Type: DEFAULT; Schema: xxxx; Owner: postgres
--

ALTER TABLE ONLY ticket_label ALTER COLUMN ticket_label_id SET DEFAULT nextval('ticket_label_ticket_label_id_seq'::regclass);


--
-- TOC entry 2886 (class 2604 OID 19888)
-- Name: ticket_log ticket_log_id; Type: DEFAULT; Schema: xxxx; Owner: postgres
--

ALTER TABLE ONLY ticket_log ALTER COLUMN ticket_log_id SET DEFAULT nextval('ticket_log_ticket_log_id_seq'::regclass);


--
-- TOC entry 2888 (class 2604 OID 19889)
-- Name: ticket_message ticket_message_id; Type: DEFAULT; Schema: xxxx; Owner: postgres
--

ALTER TABLE ONLY ticket_message ALTER COLUMN ticket_message_id SET DEFAULT nextval('ticket_message_ticket_message_id_seq'::regclass);


--
-- TOC entry 2889 (class 2604 OID 19890)
-- Name: ticket_script ticket_script_id; Type: DEFAULT; Schema: xxxx; Owner: postgres
--

ALTER TABLE ONLY ticket_script ALTER COLUMN ticket_script_id SET DEFAULT nextval('ticket_script_ticket_script_id_seq'::regclass);


--
-- TOC entry 2890 (class 2604 OID 19891)
-- Name: visitor visitor_id; Type: DEFAULT; Schema: xxxx; Owner: postgres
--

ALTER TABLE ONLY visitor ALTER COLUMN visitor_id SET DEFAULT nextval('visitor_visitor_id_seq'::regclass);


--
-- TOC entry 2893 (class 2604 OID 19892)
-- Name: wall_message wall_message_id; Type: DEFAULT; Schema: xxxx; Owner: postgres
--

ALTER TABLE ONLY wall_message ALTER COLUMN wall_message_id SET DEFAULT nextval('wall_message_wall_message_id_seq'::regclass);

--
-- TOC entry 3322 (class 0 OID 0)
-- Dependencies: 178
-- Name: account_account_id_seq; Type: SEQUENCE SET; Schema: xxxx; Owner: postgres
--

SELECT pg_catalog.setval('account_account_id_seq', 1, true);


--
-- TOC entry 3323 (class 0 OID 0)
-- Dependencies: 180
-- Name: agent_agent_id_seq; Type: SEQUENCE SET; Schema: xxxx; Owner: postgres
--

SELECT pg_catalog.setval('agent_agent_id_seq', 1, true);


--
-- TOC entry 3324 (class 0 OID 0)
-- Dependencies: 182
-- Name: agent_campaign_agent_campaign_id_seq; Type: SEQUENCE SET; Schema: xxxx; Owner: postgres
--

SELECT pg_catalog.setval('agent_campaign_agent_campaign_id_seq', 1, true);


--
-- TOC entry 3325 (class 0 OID 0)
-- Dependencies: 184
-- Name: agent_status_history_agent_status_history_id_seq; Type: SEQUENCE SET; Schema: xxxx; Owner: postgres
--

SELECT pg_catalog.setval('agent_status_history_agent_status_history_id_seq', 1, true);


--
-- TOC entry 3326 (class 0 OID 0)
-- Dependencies: 193
-- Name: campaign_campaign_id_seq; Type: SEQUENCE SET; Schema: xxxx; Owner: postgres
--

SELECT pg_catalog.setval('campaign_campaign_id_seq', 1, true);


--
-- TOC entry 3327 (class 0 OID 0)
-- Dependencies: 195
-- Name: campaign_group_campaign_group_id_seq; Type: SEQUENCE SET; Schema: xxxx; Owner: postgres
--

SELECT pg_catalog.setval('campaign_group_campaign_group_id_seq', 1, true);


--
-- TOC entry 3328 (class 0 OID 0)
-- Dependencies: 197
-- Name: campaign_script_campaign_script_id_seq; Type: SEQUENCE SET; Schema: xxxx; Owner: postgres
--

SELECT pg_catalog.setval('campaign_script_campaign_script_id_seq', 1, false);


--
-- TOC entry 3329 (class 0 OID 0)
-- Dependencies: 199
-- Name: campaign_tagging_result_campaign_tagging_result_id_seq; Type: SEQUENCE SET; Schema: xxxx; Owner: postgres
--

SELECT pg_catalog.setval('campaign_tagging_result_campaign_tagging_result_id_seq', 1, true);


--
-- TOC entry 3330 (class 0 OID 0)
-- Dependencies: 201
-- Name: cdr_cdr_id_seq; Type: SEQUENCE SET; Schema: xxxx; Owner: postgres
--

SELECT pg_catalog.setval('cdr_cdr_id_seq', 1, true);


--
-- TOC entry 3331 (class 0 OID 0)
-- Dependencies: 204
-- Name: cgroup_group_id_seq; Type: SEQUENCE SET; Schema: xxxx; Owner: postgres
--

SELECT pg_catalog.setval('cgroup_group_id_seq', 1, true);


--
-- TOC entry 3333 (class 0 OID 0)
-- Dependencies: 209
-- Name: contact_contact_id_seq; Type: SEQUENCE SET; Schema: xxxx; Owner: postgres
--

SELECT pg_catalog.setval('contact_contact_id_seq', 1, true);


--
-- TOC entry 3334 (class 0 OID 0)
-- Dependencies: 211
-- Name: contact_method_contact_method_id_seq; Type: SEQUENCE SET; Schema: xxxx; Owner: postgres
--

SELECT pg_catalog.setval('contact_method_contact_method_id_seq', 1, true);


--
-- TOC entry 3335 (class 0 OID 0)
-- Dependencies: 214
-- Name: conversation_attachment_conversation_attachment_id_seq; Type: SEQUENCE SET; Schema: xxxx; Owner: postgres
--

SELECT pg_catalog.setval('conversation_attachment_conversation_attachment_id_seq', 1, true);


--
-- TOC entry 3336 (class 0 OID 0)
-- Dependencies: 215
-- Name: conversation_conversation_id_seq; Type: SEQUENCE SET; Schema: xxxx; Owner: postgres
--

SELECT pg_catalog.setval('conversation_conversation_id_seq', 1, true);


--
-- TOC entry 3337 (class 0 OID 0)
-- Dependencies: 217
-- Name: conversation_member_conversation_member_id_seq; Type: SEQUENCE SET; Schema: xxxx; Owner: postgres
--

SELECT pg_catalog.setval('conversation_member_conversation_member_id_seq', 1, true);


--
-- TOC entry 3338 (class 0 OID 0)
-- Dependencies: 219
-- Name: conversation_message_conversation_message_id_seq; Type: SEQUENCE SET; Schema: xxxx; Owner: postgres
--

SELECT pg_catalog.setval('conversation_message_conversation_message_id_seq', 1, true);


--
-- TOC entry 3339 (class 0 OID 0)
-- Dependencies: 221
-- Name: conversation_notification_notification_id_seq; Type: SEQUENCE SET; Schema: xxxx; Owner: postgres
--

SELECT pg_catalog.setval('conversation_notification_notification_id_seq', 1, false);


--
-- TOC entry 3340 (class 0 OID 0)
-- Dependencies: 223
-- Name: dialer_dialer_id_seq; Type: SEQUENCE SET; Schema: xxxx; Owner: postgres
--

SELECT pg_catalog.setval('dialer_dialer_id_seq', 1, true);


--
-- TOC entry 3341 (class 0 OID 0)
-- Dependencies: 225
-- Name: dialer_entry_dialer_entry_id_seq; Type: SEQUENCE SET; Schema: xxxx; Owner: postgres
--

SELECT pg_catalog.setval('dialer_entry_dialer_entry_id_seq', 1, true);


--
-- TOC entry 3342 (class 0 OID 0)
-- Dependencies: 227
-- Name: label_label_id_seq; Type: SEQUENCE SET; Schema: xxxx; Owner: postgres
--

SELECT pg_catalog.setval('label_label_id_seq', 1, true);


--
-- TOC entry 3343 (class 0 OID 0)
-- Dependencies: 230
-- Name: notification_campaign_notification_campaign_id_seq; Type: SEQUENCE SET; Schema: xxxx; Owner: postgres
--

SELECT pg_catalog.setval('notification_campaign_notification_campaign_id_seq', 1, false);


--
-- TOC entry 3344 (class 0 OID 0)
-- Dependencies: 231
-- Name: notification_notification_id_seq; Type: SEQUENCE SET; Schema: xxxx; Owner: postgres
--

SELECT pg_catalog.setval('notification_notification_id_seq', 1, false);


--
-- TOC entry 3345 (class 0 OID 0)
-- Dependencies: 233
-- Name: queue_log_queue_log_id_seq; Type: SEQUENCE SET; Schema: xxxx; Owner: postgres
--

SELECT pg_catalog.setval('queue_log_queue_log_id_seq', 257661, true);


--
-- TOC entry 3346 (class 0 OID 0)
-- Dependencies: 235
-- Name: report_content_report_content_id_seq; Type: SEQUENCE SET; Schema: xxxx; Owner: postgres
--

SELECT pg_catalog.setval('report_content_report_content_id_seq', 1, true);


--
-- TOC entry 3347 (class 0 OID 0)
-- Dependencies: 237
-- Name: report_destination_report_destination_id_seq; Type: SEQUENCE SET; Schema: xxxx; Owner: postgres
--

SELECT pg_catalog.setval('report_destination_report_destination_id_seq', 1, true);


--
-- TOC entry 3348 (class 0 OID 0)
-- Dependencies: 239
-- Name: report_event_report_event_id_seq; Type: SEQUENCE SET; Schema: xxxx; Owner: postgres
--

SELECT pg_catalog.setval('report_event_report_event_id_seq', 1, true);


--
-- TOC entry 3349 (class 0 OID 0)
-- Dependencies: 241
-- Name: report_log_report_log_id_seq; Type: SEQUENCE SET; Schema: xxxx; Owner: postgres
--

SELECT pg_catalog.setval('report_log_report_log_id_seq', 1, false);


--
-- TOC entry 3350 (class 0 OID 0)
-- Dependencies: 245
-- Name: tagging_result_tagging_result_id_seq; Type: SEQUENCE SET; Schema: xxxx; Owner: postgres
--

SELECT pg_catalog.setval('tagging_result_tagging_result_id_seq', 1, true);


--
-- TOC entry 3351 (class 0 OID 0)
-- Dependencies: 246
-- Name: tagging_tagging_id_seq; Type: SEQUENCE SET; Schema: xxxx; Owner: postgres
--

SELECT pg_catalog.setval('tagging_tagging_id_seq', 1, true);


--
-- TOC entry 3352 (class 0 OID 0)
-- Dependencies: 249
-- Name: ticket_asignee_ticket_asignee_id_seq; Type: SEQUENCE SET; Schema: xxxx; Owner: postgres
--

SELECT pg_catalog.setval('ticket_asignee_ticket_asignee_id_seq', 1, true);


--
-- TOC entry 3353 (class 0 OID 0)
-- Dependencies: 251
-- Name: ticket_attachment_ticket_attachment_id_seq; Type: SEQUENCE SET; Schema: xxxx; Owner: postgres
--

SELECT pg_catalog.setval('ticket_attachment_ticket_attachment_id_seq', 1, true);


--
-- TOC entry 3354 (class 0 OID 0)
-- Dependencies: 451
-- Name: ticket_label_ticket_label_id_seq; Type: SEQUENCE SET; Schema: xxxx; Owner: postgres
--

SELECT pg_catalog.setval('ticket_label_ticket_label_id_seq', 1, true);


--
-- TOC entry 3355 (class 0 OID 0)
-- Dependencies: 253
-- Name: ticket_log_ticket_log_id_seq; Type: SEQUENCE SET; Schema: xxxx; Owner: postgres
--

SELECT pg_catalog.setval('ticket_log_ticket_log_id_seq', 1, true);


--
-- TOC entry 3356 (class 0 OID 0)
-- Dependencies: 255
-- Name: ticket_message_ticket_message_id_seq; Type: SEQUENCE SET; Schema: xxxx; Owner: postgres
--

SELECT pg_catalog.setval('ticket_message_ticket_message_id_seq', 1, true);


--
-- TOC entry 3357 (class 0 OID 0)
-- Dependencies: 257
-- Name: ticket_script_ticket_script_id_seq; Type: SEQUENCE SET; Schema: xxxx; Owner: postgres
--

SELECT pg_catalog.setval('ticket_script_ticket_script_id_seq', 1, true);


--
-- TOC entry 3358 (class 0 OID 0)
-- Dependencies: 258
-- Name: ticket_ticket_id_seq; Type: SEQUENCE SET; Schema: xxxx; Owner: postgres
--

SELECT pg_catalog.setval('ticket_ticket_id_seq', 1, true);


--
-- TOC entry 3359 (class 0 OID 0)
-- Dependencies: 260
-- Name: visitor_visitor_id_seq; Type: SEQUENCE SET; Schema: xxxx; Owner: postgres
--

SELECT pg_catalog.setval('visitor_visitor_id_seq', 1, true);


--
-- TOC entry 3360 (class 0 OID 0)
-- Dependencies: 262
-- Name: wall_message_wall_message_id_seq; Type: SEQUENCE SET; Schema: xxxx; Owner: postgres
--

SELECT pg_catalog.setval('wall_message_wall_message_id_seq', 1, true);


--
-- TOC entry 2899 (class 2606 OID 32103)
-- Name: account account_pkey; Type: CONSTRAINT; Schema: xxxx; Owner: postgres
--

ALTER TABLE ONLY account
    ADD CONSTRAINT account_pkey PRIMARY KEY (account_id);


--
-- TOC entry 2905 (class 2606 OID 32105)
-- Name: agent_campaign agent_campaign_pkey; Type: CONSTRAINT; Schema: xxxx; Owner: postgres
--

ALTER TABLE ONLY agent_campaign
    ADD CONSTRAINT agent_campaign_pkey PRIMARY KEY (agent_campaign_id);


--
-- TOC entry 2901 (class 2606 OID 32107)
-- Name: agent agent_pkey; Type: CONSTRAINT; Schema: xxxx; Owner: postgres
--

ALTER TABLE ONLY agent
    ADD CONSTRAINT agent_pkey PRIMARY KEY (agent_id);


--
-- TOC entry 2908 (class 2606 OID 32109)
-- Name: agent_status_history agent_status_history_pkey; Type: CONSTRAINT; Schema: xxxx; Owner: postgres
--

ALTER TABLE ONLY agent_status_history
    ADD CONSTRAINT agent_status_history_pkey PRIMARY KEY (agent_status_history_id);


--
-- TOC entry 2903 (class 2606 OID 32111)
-- Name: agent agent_username_key; Type: CONSTRAINT; Schema: xxxx; Owner: postgres
--

ALTER TABLE ONLY agent
    ADD CONSTRAINT agent_username_key UNIQUE (username);


--
-- TOC entry 2925 (class 2606 OID 32113)
-- Name: campaign_group campaign_group_pkey; Type: CONSTRAINT; Schema: xxxx; Owner: postgres
--

ALTER TABLE ONLY campaign_group
    ADD CONSTRAINT campaign_group_pkey PRIMARY KEY (campaign_group_id);


--
-- TOC entry 2923 (class 2606 OID 32115)
-- Name: campaign campaign_pkey; Type: CONSTRAINT; Schema: xxxx; Owner: postgres
--

ALTER TABLE ONLY campaign
    ADD CONSTRAINT campaign_pkey PRIMARY KEY (campaign_id);


--
-- TOC entry 2927 (class 2606 OID 32117)
-- Name: campaign_script campaign_script_pkey; Type: CONSTRAINT; Schema: xxxx; Owner: postgres
--

ALTER TABLE ONLY campaign_script
    ADD CONSTRAINT campaign_script_pkey PRIMARY KEY (campaign_script_id);


--
-- TOC entry 2929 (class 2606 OID 32119)
-- Name: campaign_tagging_result campaign_tagging_result_pkey; Type: CONSTRAINT; Schema: xxxx; Owner: postgres
--

ALTER TABLE ONLY campaign_tagging_result
    ADD CONSTRAINT campaign_tagging_result_pkey PRIMARY KEY (campaign_tagging_result_id);


--
-- TOC entry 2931 (class 2606 OID 32121)
-- Name: campaign_type campaign_type_pkey; Type: CONSTRAINT; Schema: xxxx; Owner: postgres
--

ALTER TABLE ONLY campaign_type
    ADD CONSTRAINT campaign_type_pkey PRIMARY KEY (campaign_type_id);


--
-- TOC entry 2912 (class 2606 OID 32123)
-- Name: cdr cdr_pkey; Type: CONSTRAINT; Schema: xxxx; Owner: postgres
--

ALTER TABLE ONLY cdr
    ADD CONSTRAINT cdr_pkey PRIMARY KEY (cdr_id);


--
-- TOC entry 2933 (class 2606 OID 32125)
-- Name: cdr_tagging cdr_tagging_pkey; Type: CONSTRAINT; Schema: xxxx; Owner: postgres
--

ALTER TABLE ONLY cdr_tagging
    ADD CONSTRAINT cdr_tagging_pkey PRIMARY KEY (uniqueid);


--
-- TOC entry 2915 (class 2606 OID 32127)
-- Name: cdr_ticket cdr_ticket_pkey; Type: CONSTRAINT; Schema: xxxx; Owner: postgres
--

ALTER TABLE ONLY cdr_ticket
    ADD CONSTRAINT cdr_ticket_pkey PRIMARY KEY (uniqueid);


--
-- TOC entry 2941 (class 2606 OID 32133)
-- Name: configuration configuration_pkey; Type: CONSTRAINT; Schema: xxxx; Owner: postgres
--

ALTER TABLE ONLY configuration
    ADD CONSTRAINT configuration_pkey PRIMARY KEY (configuration_id);


--
-- TOC entry 2945 (class 2606 OID 32135)
-- Name: contact_method contact_method_pkey; Type: CONSTRAINT; Schema: xxxx; Owner: postgres
--

ALTER TABLE ONLY contact_method
    ADD CONSTRAINT contact_method_pkey PRIMARY KEY (contact_method_id);


--
-- TOC entry 2943 (class 2606 OID 32137)
-- Name: contact contact_pkey; Type: CONSTRAINT; Schema: xxxx; Owner: postgres
--

ALTER TABLE ONLY contact
    ADD CONSTRAINT contact_pkey PRIMARY KEY (contact_id);


--
-- TOC entry 2949 (class 2606 OID 32139)
-- Name: conversation_attachment conversation_attachment_pkey; Type: CONSTRAINT; Schema: xxxx; Owner: postgres
--

ALTER TABLE ONLY conversation_attachment
    ADD CONSTRAINT conversation_attachment_pkey PRIMARY KEY (conversation_attachment_id);


--
-- TOC entry 2951 (class 2606 OID 32141)
-- Name: conversation_member conversation_member_pkey; Type: CONSTRAINT; Schema: xxxx; Owner: postgres
--

ALTER TABLE ONLY conversation_member
    ADD CONSTRAINT conversation_member_pkey PRIMARY KEY (conversation_member_id);


--
-- TOC entry 2953 (class 2606 OID 32143)
-- Name: conversation_message conversation_message_pkey; Type: CONSTRAINT; Schema: xxxx; Owner: postgres
--

ALTER TABLE ONLY conversation_message
    ADD CONSTRAINT conversation_message_pkey PRIMARY KEY (conversation_message_id);


--
-- TOC entry 2955 (class 2606 OID 32145)
-- Name: conversation_notification conversation_notification_pkey; Type: CONSTRAINT; Schema: xxxx; Owner: postgres
--

ALTER TABLE ONLY conversation_notification
    ADD CONSTRAINT conversation_notification_pkey PRIMARY KEY (notification_id);


--
-- TOC entry 2947 (class 2606 OID 32147)
-- Name: conversation conversation_pkey; Type: CONSTRAINT; Schema: xxxx; Owner: postgres
--

ALTER TABLE ONLY conversation
    ADD CONSTRAINT conversation_pkey PRIMARY KEY (conversation_id);


--
-- TOC entry 2959 (class 2606 OID 32149)
-- Name: dialer_entry dialer_entry_pkey; Type: CONSTRAINT; Schema: xxxx; Owner: postgres
--

ALTER TABLE ONLY dialer_entry
    ADD CONSTRAINT dialer_entry_pkey PRIMARY KEY (dialer_entry_id);


--
-- TOC entry 2957 (class 2606 OID 32151)
-- Name: dialer dialer_pkey; Type: CONSTRAINT; Schema: xxxx; Owner: postgres
--

ALTER TABLE ONLY dialer
    ADD CONSTRAINT dialer_pkey PRIMARY KEY (dialer_id);


--
-- TOC entry 2935 (class 2606 OID 32153)
-- Name: cgroup group_pkey; Type: CONSTRAINT; Schema: xxxx; Owner: postgres
--

ALTER TABLE ONLY cgroup
    ADD CONSTRAINT group_pkey PRIMARY KEY (group_id);


--
-- TOC entry 2961 (class 2606 OID 32155)
-- Name: label label_pkey; Type: CONSTRAINT; Schema: xxxx; Owner: postgres
--

ALTER TABLE ONLY label
    ADD CONSTRAINT label_pkey PRIMARY KEY (label_id);


--
-- TOC entry 2965 (class 2606 OID 32157)
-- Name: notification_campaign notification_campaign_pkey; Type: CONSTRAINT; Schema: xxxx; Owner: postgres
--

ALTER TABLE ONLY notification_campaign
    ADD CONSTRAINT notification_campaign_pkey PRIMARY KEY (notification_campaign_id);


--
-- TOC entry 2963 (class 2606 OID 32159)
-- Name: notification notification_pkey; Type: CONSTRAINT; Schema: xxxx; Owner: postgres
--

ALTER TABLE ONLY notification
    ADD CONSTRAINT notification_pkey PRIMARY KEY (notification_id);


--
-- TOC entry 2920 (class 2606 OID 32161)
-- Name: queue_log queue_log_pkey; Type: CONSTRAINT; Schema: xxxx; Owner: postgres
--

ALTER TABLE ONLY queue_log
    ADD CONSTRAINT queue_log_pkey PRIMARY KEY (queue_log_id);


--
-- TOC entry 2967 (class 2606 OID 32163)
-- Name: report_content report_content_pkey; Type: CONSTRAINT; Schema: xxxx; Owner: postgres
--

ALTER TABLE ONLY report_content
    ADD CONSTRAINT report_content_pkey PRIMARY KEY (report_content_id);


--
-- TOC entry 2969 (class 2606 OID 32165)
-- Name: report_destination report_destination_pkey; Type: CONSTRAINT; Schema: xxxx; Owner: postgres
--

ALTER TABLE ONLY report_destination
    ADD CONSTRAINT report_destination_pkey PRIMARY KEY (report_destination_id);


--
-- TOC entry 2971 (class 2606 OID 32167)
-- Name: report_event report_event_pkey; Type: CONSTRAINT; Schema: xxxx; Owner: postgres
--

ALTER TABLE ONLY report_event
    ADD CONSTRAINT report_event_pkey PRIMARY KEY (report_event_id);


--
-- TOC entry 2973 (class 2606 OID 32169)
-- Name: report_log report_log_pkey; Type: CONSTRAINT; Schema: xxxx; Owner: postgres
--

ALTER TABLE ONLY report_log
    ADD CONSTRAINT report_log_pkey PRIMARY KEY (report_log_id);


--
-- TOC entry 2975 (class 2606 OID 32171)
-- Name: status_code status_code_pkey; Type: CONSTRAINT; Schema: xxxx; Owner: postgres
--

ALTER TABLE ONLY status_code
    ADD CONSTRAINT status_code_pkey PRIMARY KEY (status_code_id);


--
-- TOC entry 2978 (class 2606 OID 32173)
-- Name: tagging tagging_pkey; Type: CONSTRAINT; Schema: xxxx; Owner: postgres
--

ALTER TABLE ONLY tagging
    ADD CONSTRAINT tagging_pkey PRIMARY KEY (tagging_id);


--
-- TOC entry 2980 (class 2606 OID 32175)
-- Name: tagging_result tagging_result_pkey; Type: CONSTRAINT; Schema: xxxx; Owner: postgres
--

ALTER TABLE ONLY tagging_result
    ADD CONSTRAINT tagging_result_pkey PRIMARY KEY (tagging_result_id);


--
-- TOC entry 2996 (class 2606 OID 32177)
-- Name: ticket_asignee ticket_asignee_pkey; Type: CONSTRAINT; Schema: xxxx; Owner: postgres
--

ALTER TABLE ONLY ticket_asignee
    ADD CONSTRAINT ticket_asignee_pkey PRIMARY KEY (ticket_asignee_id);


--
-- TOC entry 2998 (class 2606 OID 32179)
-- Name: ticket_attachment ticket_attachment_pkey; Type: CONSTRAINT; Schema: xxxx; Owner: postgres
--

ALTER TABLE ONLY ticket_attachment
    ADD CONSTRAINT ticket_attachment_pkey PRIMARY KEY (ticket_attachment_id);


--
-- TOC entry 3001 (class 2606 OID 32181)
-- Name: ticket_log ticket_log_pkey; Type: CONSTRAINT; Schema: xxxx; Owner: postgres
--

ALTER TABLE ONLY ticket_log
    ADD CONSTRAINT ticket_log_pkey PRIMARY KEY (ticket_log_id);


--
-- TOC entry 3010 (class 2606 OID 32183)
-- Name: ticket_message ticket_message_pkey; Type: CONSTRAINT; Schema: xxxx; Owner: postgres
--

ALTER TABLE ONLY ticket_message
    ADD CONSTRAINT ticket_message_pkey PRIMARY KEY (ticket_message_id);


--
-- TOC entry 2989 (class 2606 OID 32185)
-- Name: ticket ticket_pkey; Type: CONSTRAINT; Schema: xxxx; Owner: postgres
--

ALTER TABLE ONLY ticket
    ADD CONSTRAINT ticket_pkey PRIMARY KEY (ticket_id);


--
-- TOC entry 3015 (class 2606 OID 32187)
-- Name: ticket_script ticket_script_pkey; Type: CONSTRAINT; Schema: xxxx; Owner: postgres
--

ALTER TABLE ONLY ticket_script
    ADD CONSTRAINT ticket_script_pkey PRIMARY KEY (ticket_script_id);


--
-- TOC entry 3017 (class 2606 OID 32189)
-- Name: visitor visitor_pkey; Type: CONSTRAINT; Schema: xxxx; Owner: postgres
--

ALTER TABLE ONLY visitor
    ADD CONSTRAINT visitor_pkey PRIMARY KEY (visitor_id);


--
-- TOC entry 3019 (class 2606 OID 32191)
-- Name: visitor visitor_visitor_uuid_unique; Type: CONSTRAINT; Schema: xxxx; Owner: postgres
--

ALTER TABLE ONLY visitor
    ADD CONSTRAINT visitor_visitor_uuid_unique UNIQUE (visitor_uuid);


--
-- TOC entry 3021 (class 2606 OID 32193)
-- Name: wall_message wall_message_pkey; Type: CONSTRAINT; Schema: xxxx; Owner: postgres
--

ALTER TABLE ONLY wall_message
    ADD CONSTRAINT wall_message_pkey PRIMARY KEY (wall_message_id);


--
-- TOC entry 2906 (class 1259 OID 32720)
-- Name: agent_status_history_datetime_ix; Type: INDEX; Schema: xxxx; Owner: postgres
--

CREATE INDEX agent_status_history_datetime_ix ON agent_status_history USING btree (datetime);


--
-- TOC entry 2909 (class 1259 OID 32721)
-- Name: cdr_calldate_idx; Type: INDEX; Schema: xxxx; Owner: postgres
--

CREATE INDEX cdr_calldate_idx ON cdr USING btree (calldate);


--
-- TOC entry 2910 (class 1259 OID 32722)
-- Name: cdr_dst_idx; Type: INDEX; Schema: xxxx; Owner: postgres
--

CREATE INDEX cdr_dst_idx ON cdr USING btree (dst);


--
-- TOC entry 2913 (class 1259 OID 32723)
-- Name: cdr_uniqueid_idx; Type: INDEX; Schema: xxxx; Owner: postgres
--

CREATE INDEX cdr_uniqueid_idx ON cdr USING btree (uniqueid);


--
-- TOC entry 2916 (class 1259 OID 32724)
-- Name: queue_log_callid_ix; Type: INDEX; Schema: xxxx; Owner: postgres
--

CREATE INDEX queue_log_callid_ix ON queue_log USING btree (callid);


--
-- TOC entry 2917 (class 1259 OID 32725)
-- Name: queue_log_created_ix; Type: INDEX; Schema: xxxx; Owner: postgres
--

CREATE INDEX queue_log_created_ix ON queue_log USING btree (created);


--
-- TOC entry 2918 (class 1259 OID 32726)
-- Name: queue_log_event_idx; Type: INDEX; Schema: xxxx; Owner: postgres
--

CREATE INDEX queue_log_event_idx ON queue_log USING btree (event);


--
-- TOC entry 2921 (class 1259 OID 32727)
-- Name: queue_log_queuename_ix; Type: INDEX; Schema: xxxx; Owner: postgres
--

CREATE INDEX queue_log_queuename_ix ON queue_log USING btree (queuename);


--
-- TOC entry 2976 (class 1259 OID 32728)
-- Name: tagging_datetime_ix; Type: INDEX; Schema: xxxx; Owner: postgres
--

CREATE INDEX tagging_datetime_ix ON tagging USING btree (datetime);


--
-- TOC entry 2981 (class 1259 OID 32729)
-- Name: ticket_agent_id_idx; Type: INDEX; Schema: xxxx; Owner: postgres
--

CREATE INDEX ticket_agent_id_idx ON ticket USING btree (agent_id);


--
-- TOC entry 2982 (class 1259 OID 32730)
-- Name: ticket_campaign_id_idx; Type: INDEX; Schema: xxxx; Owner: postgres
--

CREATE INDEX ticket_campaign_id_idx ON ticket USING btree (campaign_id);


--
-- TOC entry 2983 (class 1259 OID 32731)
-- Name: ticket_channel_type_idx; Type: INDEX; Schema: xxxx; Owner: postgres
--

CREATE INDEX ticket_channel_type_idx ON ticket USING btree (channel_type);


--
-- TOC entry 2984 (class 1259 OID 65636)
-- Name: ticket_closed_date_idx; Type: INDEX; Schema: xxxx; Owner: postgres
--

CREATE INDEX ticket_closed_date_idx ON ticket USING btree (closed_date);


--
-- TOC entry 2985 (class 1259 OID 32733)
-- Name: ticket_created_date_idx; Type: INDEX; Schema: xxxx; Owner: postgres
--

CREATE INDEX ticket_created_date_idx ON ticket USING btree (created_date);


--
-- TOC entry 2986 (class 1259 OID 32734)
-- Name: ticket_is_public_idx; Type: INDEX; Schema: xxxx; Owner: postgres
--

CREATE INDEX ticket_is_public_idx ON ticket USING btree (is_public);


--
-- TOC entry 2987 (class 1259 OID 32735)
-- Name: ticket_last_message_date_idx; Type: INDEX; Schema: xxxx; Owner: postgres
--

CREATE INDEX ticket_last_message_date_idx ON ticket USING btree (last_message_date);


--
-- TOC entry 2999 (class 1259 OID 32736)
-- Name: ticket_log_agent_id_idx; Type: INDEX; Schema: xxxx; Owner: postgres
--

CREATE INDEX ticket_log_agent_id_idx ON ticket_log USING btree (agent_id);


--
-- TOC entry 3002 (class 1259 OID 32737)
-- Name: ticket_log_ticket_id_idx; Type: INDEX; Schema: xxxx; Owner: postgres
--

CREATE INDEX ticket_log_ticket_id_idx ON ticket_log USING btree (ticket_id);


--
-- TOC entry 3003 (class 1259 OID 32738)
-- Name: ticket_message_agent_id_idx; Type: INDEX; Schema: xxxx; Owner: postgres
--

CREATE INDEX ticket_message_agent_id_idx ON ticket_message USING btree (agent_id);


--
-- TOC entry 3004 (class 1259 OID 32739)
-- Name: ticket_message_channel_type_idx; Type: INDEX; Schema: xxxx; Owner: postgres
--

CREATE INDEX ticket_message_channel_type_idx ON ticket_message USING btree (channel_type);


--
-- TOC entry 3005 (class 1259 OID 32740)
-- Name: ticket_message_created_date_idx; Type: INDEX; Schema: xxxx; Owner: postgres
--

CREATE INDEX ticket_message_created_date_idx ON ticket_message USING btree (created_date);


--
-- TOC entry 3006 (class 1259 OID 32741)
-- Name: ticket_message_deleted_idx; Type: INDEX; Schema: xxxx; Owner: postgres
--

CREATE INDEX ticket_message_deleted_idx ON ticket_message USING btree (deleted);


--
-- TOC entry 3007 (class 1259 OID 32742)
-- Name: ticket_message_has_attachments_idx; Type: INDEX; Schema: xxxx; Owner: postgres
--

CREATE INDEX ticket_message_has_attachments_idx ON ticket_message USING btree (has_attachments);


--
-- TOC entry 3008 (class 1259 OID 32743)
-- Name: ticket_message_internal_idx; Type: INDEX; Schema: xxxx; Owner: postgres
--

CREATE INDEX ticket_message_internal_idx ON ticket_message USING btree (internal);


--
-- TOC entry 3011 (class 1259 OID 32744)
-- Name: ticket_message_private_subticket_id_idx; Type: INDEX; Schema: xxxx; Owner: postgres
--

CREATE INDEX ticket_message_private_subticket_id_idx ON ticket_message USING btree (private_subticket_id);


--
-- TOC entry 3012 (class 1259 OID 32745)
-- Name: ticket_message_public_subticket_id_idx; Type: INDEX; Schema: xxxx; Owner: postgres
--

CREATE INDEX ticket_message_public_subticket_id_idx ON ticket_message USING btree (public_subticket_id);


--
-- TOC entry 3013 (class 1259 OID 32746)
-- Name: ticket_message_ticket_id_idx; Type: INDEX; Schema: xxxx; Owner: postgres
--

CREATE INDEX ticket_message_ticket_id_idx ON ticket_message USING btree (ticket_id);


--
-- TOC entry 2990 (class 1259 OID 32747)
-- Name: ticket_reopened_date_idx; Type: INDEX; Schema: xxxx; Owner: postgres
--

CREATE INDEX ticket_reopened_date_idx ON ticket USING btree (reopened_date);


--
-- TOC entry 2991 (class 1259 OID 32748)
-- Name: ticket_source_type_idx; Type: INDEX; Schema: xxxx; Owner: postgres
--

CREATE INDEX ticket_source_type_idx ON ticket USING btree (source_type);


--
-- TOC entry 2992 (class 1259 OID 32749)
-- Name: ticket_tagging_result_id_idx; Type: INDEX; Schema: xxxx; Owner: postgres
--

CREATE INDEX ticket_tagging_result_id_idx ON ticket USING btree (tagging_result_id);


--
-- TOC entry 2993 (class 1259 OID 32750)
-- Name: ticket_ticket_status_idx; Type: INDEX; Schema: xxxx; Owner: postgres
--

CREATE INDEX ticket_ticket_status_idx ON ticket USING btree (ticket_status);


--
-- TOC entry 2994 (class 1259 OID 32751)
-- Name: ticket_updated_date_idx; Type: INDEX; Schema: xxxx; Owner: postgres
--

CREATE INDEX ticket_updated_date_idx ON ticket USING btree (updated_date);


--
-- TOC entry 3024 (class 2606 OID 33443)
-- Name: agent_campaign agent_campaign_agent_id_fkey; Type: FK CONSTRAINT; Schema: xxxx; Owner: postgres
--

ALTER TABLE ONLY agent_campaign
    ADD CONSTRAINT agent_campaign_agent_id_fkey FOREIGN KEY (agent_id) REFERENCES agent(agent_id) ON DELETE CASCADE;


--
-- TOC entry 3023 (class 2606 OID 33448)
-- Name: agent_campaign agent_campaign_campaign_id_fkey; Type: FK CONSTRAINT; Schema: xxxx; Owner: postgres
--

ALTER TABLE ONLY agent_campaign
    ADD CONSTRAINT agent_campaign_campaign_id_fkey FOREIGN KEY (campaign_id) REFERENCES campaign(campaign_id) ON DELETE CASCADE;


--
-- TOC entry 3022 (class 2606 OID 33453)
-- Name: agent agent_group_id_fkey; Type: FK CONSTRAINT; Schema: xxxx; Owner: postgres
--

ALTER TABLE ONLY agent
    ADD CONSTRAINT agent_group_id_fkey FOREIGN KEY (group_id) REFERENCES cgroup(group_id);


--
-- TOC entry 3027 (class 2606 OID 33458)
-- Name: agent_status_history agent_status_history_agent_id_fkey; Type: FK CONSTRAINT; Schema: xxxx; Owner: postgres
--

ALTER TABLE ONLY agent_status_history
    ADD CONSTRAINT agent_status_history_agent_id_fkey FOREIGN KEY (agent_id) REFERENCES agent(agent_id) ON DELETE CASCADE;


--
-- TOC entry 3026 (class 2606 OID 33463)
-- Name: agent_status_history agent_status_history_campaign_id_fkey; Type: FK CONSTRAINT; Schema: xxxx; Owner: postgres
--

ALTER TABLE ONLY agent_status_history
    ADD CONSTRAINT agent_status_history_campaign_id_fkey FOREIGN KEY (campaign_id) REFERENCES campaign(campaign_id) ON DELETE SET NULL;


--
-- TOC entry 3025 (class 2606 OID 33468)
-- Name: agent_status_history agent_status_history_status_code_id_fkey; Type: FK CONSTRAINT; Schema: xxxx; Owner: postgres
--

ALTER TABLE ONLY agent_status_history
    ADD CONSTRAINT agent_status_history_status_code_id_fkey FOREIGN KEY (status_code_id) REFERENCES status_code(status_code_id);


--
-- TOC entry 3029 (class 2606 OID 33473)
-- Name: campaign campaign_account_id_fkey; Type: FK CONSTRAINT; Schema: xxxx; Owner: postgres
--

ALTER TABLE ONLY campaign
    ADD CONSTRAINT campaign_account_id_fkey FOREIGN KEY (account_id) REFERENCES account(account_id);


--
-- TOC entry 3028 (class 2606 OID 33478)
-- Name: campaign campaign_campaign_type_id_fkey; Type: FK CONSTRAINT; Schema: xxxx; Owner: postgres
--

ALTER TABLE ONLY campaign
    ADD CONSTRAINT campaign_campaign_type_id_fkey FOREIGN KEY (campaign_type_id) REFERENCES campaign_type(campaign_type_id);


--
-- TOC entry 3031 (class 2606 OID 33483)
-- Name: campaign_group campaign_group_campaign_id_fkey; Type: FK CONSTRAINT; Schema: xxxx; Owner: postgres
--

ALTER TABLE ONLY campaign_group
    ADD CONSTRAINT campaign_group_campaign_id_fkey FOREIGN KEY (campaign_id) REFERENCES campaign(campaign_id);


--
-- TOC entry 3030 (class 2606 OID 33488)
-- Name: campaign_group campaign_group_group_id_fkey; Type: FK CONSTRAINT; Schema: xxxx; Owner: postgres
--

ALTER TABLE ONLY campaign_group
    ADD CONSTRAINT campaign_group_group_id_fkey FOREIGN KEY (group_id) REFERENCES cgroup(group_id);


--
-- TOC entry 3032 (class 2606 OID 33493)
-- Name: campaign_script campaign_script_campaign_id_fkey; Type: FK CONSTRAINT; Schema: xxxx; Owner: postgres
--

ALTER TABLE ONLY campaign_script
    ADD CONSTRAINT campaign_script_campaign_id_fkey FOREIGN KEY (campaign_id) REFERENCES campaign(campaign_id) ON DELETE CASCADE;


--
-- TOC entry 3034 (class 2606 OID 33498)
-- Name: campaign_tagging_result campaign_tagging_result_campaign_id_fkey; Type: FK CONSTRAINT; Schema: xxxx; Owner: postgres
--

ALTER TABLE ONLY campaign_tagging_result
    ADD CONSTRAINT campaign_tagging_result_campaign_id_fkey FOREIGN KEY (campaign_id) REFERENCES campaign(campaign_id);


--
-- TOC entry 3033 (class 2606 OID 33503)
-- Name: campaign_tagging_result campaign_tagging_result_tagging_result_id_fkey; Type: FK CONSTRAINT; Schema: xxxx; Owner: postgres
--

ALTER TABLE ONLY campaign_tagging_result
    ADD CONSTRAINT campaign_tagging_result_tagging_result_id_fkey FOREIGN KEY (tagging_result_id) REFERENCES tagging_result(tagging_result_id);

--
-- TOC entry 3039 (class 2606 OID 33528)
-- Name: contact_method contact_method_contact_id_fkey; Type: FK CONSTRAINT; Schema: xxxx; Owner: postgres
--

ALTER TABLE ONLY contact_method
    ADD CONSTRAINT contact_method_contact_id_fkey FOREIGN KEY (contact_id) REFERENCES contact(contact_id);


--
-- TOC entry 3040 (class 2606 OID 33533)
-- Name: conversation_attachment conversation_attachment_conversation_message_id_fkey; Type: FK CONSTRAINT; Schema: xxxx; Owner: postgres
--

ALTER TABLE ONLY conversation_attachment
    ADD CONSTRAINT conversation_attachment_conversation_message_id_fkey FOREIGN KEY (conversation_message_id) REFERENCES conversation_message(conversation_message_id);


--
-- TOC entry 3041 (class 2606 OID 33538)
-- Name: conversation_member conversation_member_conversation_id_fkey; Type: FK CONSTRAINT; Schema: xxxx; Owner: postgres
--

ALTER TABLE ONLY conversation_member
    ADD CONSTRAINT conversation_member_conversation_id_fkey FOREIGN KEY (conversation_id) REFERENCES conversation(conversation_id);


--
-- TOC entry 3042 (class 2606 OID 33543)
-- Name: conversation_message conversation_message_conversation_id_fkey; Type: FK CONSTRAINT; Schema: xxxx; Owner: postgres
--

ALTER TABLE ONLY conversation_message
    ADD CONSTRAINT conversation_message_conversation_id_fkey FOREIGN KEY (conversation_id) REFERENCES conversation(conversation_id);


--
-- TOC entry 3043 (class 2606 OID 33548)
-- Name: conversation_notification conversation_notification_conversation_id_fkey; Type: FK CONSTRAINT; Schema: xxxx; Owner: postgres
--

ALTER TABLE ONLY conversation_notification
    ADD CONSTRAINT conversation_notification_conversation_id_fkey FOREIGN KEY (conversation_id) REFERENCES conversation(conversation_id);


--
-- TOC entry 3044 (class 2606 OID 33553)
-- Name: dialer dialer_campaign_id_fkey; Type: FK CONSTRAINT; Schema: xxxx; Owner: postgres
--

ALTER TABLE ONLY dialer
    ADD CONSTRAINT dialer_campaign_id_fkey FOREIGN KEY (campaign_id) REFERENCES campaign(campaign_id);


--
-- TOC entry 3045 (class 2606 OID 33558)
-- Name: dialer_entry dialer_entry_campaign_id_fkey; Type: FK CONSTRAINT; Schema: xxxx; Owner: postgres
--

ALTER TABLE ONLY dialer_entry
    ADD CONSTRAINT dialer_entry_campaign_id_fkey FOREIGN KEY (campaign_id) REFERENCES campaign(campaign_id);


--
-- TOC entry 3059 (class 2606 OID 33563)
-- Name: ticket fk_tagging_result; Type: FK CONSTRAINT; Schema: xxxx; Owner: postgres
--

ALTER TABLE ONLY ticket
    ADD CONSTRAINT fk_tagging_result FOREIGN KEY (tagging_result_id) REFERENCES tagging_result(tagging_result_id);


--
-- TOC entry 3048 (class 2606 OID 33568)
-- Name: notification_campaign notification_campaign_conversation_id_fkey; Type: FK CONSTRAINT; Schema: xxxx; Owner: postgres
--

ALTER TABLE ONLY notification_campaign
    ADD CONSTRAINT notification_campaign_conversation_id_fkey FOREIGN KEY (conversation_id) REFERENCES conversation(conversation_id);


--
-- TOC entry 3047 (class 2606 OID 33573)
-- Name: notification_campaign notification_campaign_notification_id_fkey; Type: FK CONSTRAINT; Schema: xxxx; Owner: postgres
--

ALTER TABLE ONLY notification_campaign
    ADD CONSTRAINT notification_campaign_notification_id_fkey FOREIGN KEY (notification_id) REFERENCES notification(notification_id);


--
-- TOC entry 3046 (class 2606 OID 33578)
-- Name: notification notification_from_id_fkey; Type: FK CONSTRAINT; Schema: xxxx; Owner: postgres
--

ALTER TABLE ONLY notification
    ADD CONSTRAINT notification_from_id_fkey FOREIGN KEY (from_id) REFERENCES agent(agent_id);


--
-- TOC entry 3049 (class 2606 OID 33583)
-- Name: report_content report_content_report_event_id_fkey; Type: FK CONSTRAINT; Schema: xxxx; Owner: postgres
--

ALTER TABLE ONLY report_content
    ADD CONSTRAINT report_content_report_event_id_fkey FOREIGN KEY (report_event_id) REFERENCES report_event(report_event_id);


--
-- TOC entry 3050 (class 2606 OID 33588)
-- Name: report_destination report_destination_report_event_id_fkey; Type: FK CONSTRAINT; Schema: xxxx; Owner: postgres
--

ALTER TABLE ONLY report_destination
    ADD CONSTRAINT report_destination_report_event_id_fkey FOREIGN KEY (report_event_id) REFERENCES report_event(report_event_id);


--
-- TOC entry 3051 (class 2606 OID 33593)
-- Name: report_event report_event_group_id_fkey; Type: FK CONSTRAINT; Schema: xxxx; Owner: postgres
--

ALTER TABLE ONLY report_event
    ADD CONSTRAINT report_event_group_id_fkey FOREIGN KEY (group_id) REFERENCES cgroup(group_id);


--
-- TOC entry 3052 (class 2606 OID 33598)
-- Name: report_log report_log_report_event_id_fkey; Type: FK CONSTRAINT; Schema: xxxx; Owner: postgres
--

ALTER TABLE ONLY report_log
    ADD CONSTRAINT report_log_report_event_id_fkey FOREIGN KEY (report_event_id) REFERENCES report_event(report_event_id);


--
-- TOC entry 3055 (class 2606 OID 33603)
-- Name: tagging tagging_agent_id_fkey; Type: FK CONSTRAINT; Schema: xxxx; Owner: postgres
--

ALTER TABLE ONLY tagging
    ADD CONSTRAINT tagging_agent_id_fkey FOREIGN KEY (agent_id) REFERENCES agent(agent_id);


--
-- TOC entry 3054 (class 2606 OID 33608)
-- Name: tagging tagging_campaign_id_fkey; Type: FK CONSTRAINT; Schema: xxxx; Owner: postgres
--

ALTER TABLE ONLY tagging
    ADD CONSTRAINT tagging_campaign_id_fkey FOREIGN KEY (campaign_id) REFERENCES campaign(campaign_id);


--
-- TOC entry 3053 (class 2606 OID 33613)
-- Name: tagging tagging_tagging_result_id_fkey; Type: FK CONSTRAINT; Schema: xxxx; Owner: postgres
--

ALTER TABLE ONLY tagging
    ADD CONSTRAINT tagging_tagging_result_id_fkey FOREIGN KEY (tagging_result_id) REFERENCES tagging_result(tagging_result_id) ON DELETE CASCADE;


--
-- TOC entry 3058 (class 2606 OID 33618)
-- Name: ticket ticket_agent_id_fkey; Type: FK CONSTRAINT; Schema: xxxx; Owner: postgres
--

ALTER TABLE ONLY ticket
    ADD CONSTRAINT ticket_agent_id_fkey FOREIGN KEY (agent_id) REFERENCES agent(agent_id);


--
-- TOC entry 3060 (class 2606 OID 33623)
-- Name: ticket_asignee ticket_asignee_agent_id_fkey; Type: FK CONSTRAINT; Schema: xxxx; Owner: postgres
--

ALTER TABLE ONLY ticket_asignee
    ADD CONSTRAINT ticket_asignee_agent_id_fkey FOREIGN KEY (agent_id) REFERENCES agent(agent_id);


--
-- TOC entry 3061 (class 2606 OID 33628)
-- Name: ticket_attachment ticket_attachment_ticket_message_id_fkey; Type: FK CONSTRAINT; Schema: xxxx; Owner: postgres
--

ALTER TABLE ONLY ticket_attachment
    ADD CONSTRAINT ticket_attachment_ticket_message_id_fkey FOREIGN KEY (ticket_message_id) REFERENCES ticket_message(ticket_message_id);


--
-- TOC entry 3057 (class 2606 OID 33633)
-- Name: ticket ticket_campaign_id_fkey; Type: FK CONSTRAINT; Schema: xxxx; Owner: postgres
--

ALTER TABLE ONLY ticket
    ADD CONSTRAINT ticket_campaign_id_fkey FOREIGN KEY (campaign_id) REFERENCES campaign(campaign_id);


--
-- TOC entry 3056 (class 2606 OID 33638)
-- Name: ticket ticket_contact_id_fkey; Type: FK CONSTRAINT; Schema: xxxx; Owner: postgres
--

ALTER TABLE ONLY ticket
    ADD CONSTRAINT ticket_contact_id_fkey FOREIGN KEY (contact_id) REFERENCES contact(contact_id);


--
-- TOC entry 3063 (class 2606 OID 33643)
-- Name: ticket_log ticket_log_agent_id_fkey; Type: FK CONSTRAINT; Schema: xxxx; Owner: postgres
--

ALTER TABLE ONLY ticket_log
    ADD CONSTRAINT ticket_log_agent_id_fkey FOREIGN KEY (agent_id) REFERENCES agent(agent_id);


--
-- TOC entry 3062 (class 2606 OID 33648)
-- Name: ticket_log ticket_log_ticket_id_fkey; Type: FK CONSTRAINT; Schema: xxxx; Owner: postgres
--

ALTER TABLE ONLY ticket_log
    ADD CONSTRAINT ticket_log_ticket_id_fkey FOREIGN KEY (ticket_id) REFERENCES ticket(ticket_id);


--
-- TOC entry 3064 (class 2606 OID 33653)
-- Name: ticket_message ticket_message_ticket_id_fkey; Type: FK CONSTRAINT; Schema: xxxx; Owner: postgres
--

ALTER TABLE ONLY ticket_message
    ADD CONSTRAINT ticket_message_ticket_id_fkey FOREIGN KEY (ticket_id) REFERENCES ticket(ticket_id);


--
-- TOC entry 3065 (class 2606 OID 33658)
-- Name: wall_message wall_message_agent_id_fkey; Type: FK CONSTRAINT; Schema: xxxx; Owner: postgres
--

ALTER TABLE ONLY wall_message
    ADD CONSTRAINT wall_message_agent_id_fkey FOREIGN KEY (agent_id) REFERENCES agent(agent_id) ON DELETE CASCADE;

ALTER TABLE Account ADD COLUMN mail_processed_folder VARCHAR;

INSERT into campaign_type(campaign_type_id,name) values ('CHT', 'Chat');
INSERT into  campaign_type(campaign_type_id,name) values ('TIK', 'Ticket');
INSERT into  campaign_type(campaign_type_id,name) values ('IBD', 'Inbound Dialer');
INSERT into  campaign_type(campaign_type_id,name) values ('IBA', 'ACD Dialer');
INSERT into  campaign_type(campaign_type_id,name) values ('VDO', 'Video Chat');


INSERT into  status_code (status_code_id, name, billable, deleted) values('OFL', 'Offline', False, False);
INSERT into  status_code (status_code_id, name, billable, deleted) values('AVL', 'Disponible', False, False);
INSERT into  status_code (status_code_id, name, billable, deleted) values('BSY', 'Ocupado', False, False);

INSERT INTO  configuration VALUES (1, '{"sites":[],"mask":1,"colors":{"banner":{"text":"hsl(0, 0%, 100%)","background":"hsl(48, 89%, 50%)"},"button":{"text":"hsl(0, 0%, 100%)","background":"hsl(48, 89%, 50%)"},"agentDialog":{"text":"hsl(0, 0%, 0%)","background":"hsl(60, 74%, 87%)"},"clientDialog":{"text":"hsl(0, 0%, 0%)","background":"hsl(6, 63%, 82%)"}},"labels":{"startTitle":"Habla con nosostros","waitTopDescription":"Por favor espere su turno","waitBottomDescription":"Estamos a su servicio","message":"En este momento no estamos disponibles"}}');

INSERT INTO agent(
	lastname, firstname, username, deleted, role, last_seen_wall_date, answer_type)
	VALUES ('Admin', 'Conversso', '$email', false, 'ROLE_ADMIN', now(), 1);

